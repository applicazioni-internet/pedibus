# Pedibus

Questo repository contiene la soluzione al progetto proposto durante il corso di _Applicazioni Internet_ sostenuto presso il **Politecnico di Torino**, tenuto dai professori Antonio Servetti e Giovanni Malnati durante l'anno accademico 2019-2020.

## Sommario

[[_TOC_]]

---

## Overview

L’applicazione dovrà realizzare l’interfaccia ed il backend di un’applicazione web per la gestione di una forma di trasporto scolastico denominata Pedibus. Per Pedibus si intende sostanzialmente l’accompagnamento a scuola a piedi dei bambini, generalmente delle elementari, da parte di accompagnatori adulti. Gli accompagnatori "prelevano" e "consegnano" i bambini ai genitori
presso apposite fermate e insieme ai bambini percorrono un determinato tragitto per andare e tornare da scuola.

Compito dell’applicazione è quello di fornire supporto alla gestione e organizzazione del pedibus in particolare per

  1. Gestire gli utenti e fornire loro informazioni;
  2. Tenere traccia dei bambini "caricati" e "scaricati" dal pedibus;
  3. Raccogliere le disponibilità degli accompagnatori e predisporre i turni di accompagnamento.

## Dati

L’applicazione dovrà gestire i seguenti dati.

### Utenti

L’applicazione permetterà la registrazione ed il login degli utenti. Gli utenti si distinguono in accompagnatori, gli adulti, e in passeggeri, i bambini. I bambini non si registrano in prima persona, ma si registrerà per loro, con il loro nominativo, un genitore (che può avere più figli registrati). A ciò si aggiunge la possibilità per uno o più accompagnatori di essere amministratori del sistema.Compito esclusivo dell’amministratore è quello di definire (e cambiare) i turni del pedibus.

L’amministratore “master” è quello definito nel file della linea del pedibus. Gli amministratori possono promuovere altri accompagnatori ad amministratori, o declassare gli amministratori a semplici accompagnatori.

### Presenze

L’applicazione del pedibus viene utilizzata per tenere traccia della salita e della discesa dei bambini dal pedibus stesso. È quindi necessario che gli accompagnatori possano registrare su di essa in tempo reale la salita e discesa dei bambini. Tali informazioni devono essere sia visualizzabili in tempo reale sia esportabili al di fuori dell’applicazione (in un file json, csv, etc.).

Per comodità la selezione dei bambini “presenti” viene fatta a partire dall’elenco dei bambini iscritti dove quelli prenotati sono evidenziati per distinguerli dagli altri e individuarli più comodamente. È comunque possibile segnare come presente un bambino, anche se non prenotato (nel caso i genitori non l’avessero segnato per distrazione).

### Percorso o “linea”

Il percorso del pedibus è costituito da un certo numero di fermate disposte lungo un tragitto e quindi ordinate (in un verso all’andata e nel verso opposto al ritorno). Le fermate hanno associato un nome, un orario di riferimento (sia per l’andata sia per il ritorno), una posizione GPS. Il percorso può essere descritto in un file esterno e caricato dal sistema. L’applicativo caricherà tutti i percorsi da gestire a partire da appositi file presenti in una cartella del sistema. Il nome del file corrisponderà all’URL della linea e nel file, oltre al percorso, sarà anche definito almeno un indirizzo email dell’amministratore della linea.

### Corse

Le corse del pedibus sono a cadenza giornaliera, l’andata al mattino e il ritorno a pranzo o nel pomeriggio. E’ necessario quindi gestire un elenco delle corse, ciascuna associata ad un giorno dell’anno e con la distinzione tra andata e ritorno.

### Prenotazioni

Per migliorare la gestione del servizio pedibus (e non “dimenticarsi” nessuno per strada) è necessario predisporre per ogni corsa del pedibus la possibilità di registrare chi vi prenderà parte.

Chiamiamo questa funzionalità “prenotazioni”. Il genitore, per ogni corsa del pedibus potrà indicare se il figlio (o i figli) saranno presenti o no, e a partire da / fino a quale fermata. La possibilità di aggiungere o rimuovere un bambino è possibile fino al passaggio del pedibus per quella fermata.

### Disponibilità e turni

Per organizzare i turni degli accompagnatori è necessario definire e gestire due fasi distinte. La prima in cui gli accompagnatori indicano la loro disponibilità per le corse del pedibus (giorno, andata o ritorno) e la seconda in cui l’amministratore (o gli amministratori) del servizio selezionano tra i disponibili gli accompagnatori che effettivamente verranno utilizzati, definendo quindi i turni di ciascun accompagnatore per ciascuna corsa del pedibus. A fronte della definizione o della modifica di un turno, le persone coinvolte vengono avvisate tramite un messaggio interno all’applicazione del loro impegno.

## Viste

L’applicazione gestirà le seguenti schermate.

### Registrazione utente e login

La registrazione di un utente nel sistema è possibile solo agli amministratori i quali potranno aggiungere l’indirizzo email dell’utente e definirne il ruolo. All’utente arriverà di conseguenza una mail con un link per completare la registrazione del proprio profilo (es. password e altri dati).

All’atto della “creazione” di una nuova linea il sistema manderà analoga mail all’indirizzo definito come amministratore della linea (se non già presente e amministratore della stessa). E’ anche necessario definire una interfaccia per il login/logout dell’utente, tramite la password registrata.

### Interfaccia bambino: prenotazioni

Per gestire le prenotazioni il genitore ha a disposizione un elenco delle corse del pedibus (giorno e andata/ritorno) e tramite un click potrà attivare o disattivare la prenotazione del bambino per ciascuna corsa, per indicare se sarà presente o no. La prenotazione è modificabile soltanto entro il passaggio del pedibus presso la fermata. Il bambino viene assegnato ad una fermata di default configurata nel profilo, ma è possibile modificarla per ogni corsa.

### Interfaccia accompagnatore: presenze

Per gestire le presenze, gli amministratori avranno, per ogni corsa, una schermata con l’elenco di tutti i bambini iscritti associati alla fermata prescelta dai loro genitori. Qui i bambini prenotati per quella corsa saranno opportunamente evidenziati. Cliccando sul nome del bambino l’accompagnatore può indicare la presa in carico (dal genitore, all’andata, o dalla scuola, al ritorno). I nomi dei bambini, una volta presi in carico verranno opportunamente evidenziati (es. cambiandone il colore del nome). Possono essere segnati come presenti anche bambini non prenotati.

### Interfaccia accompagnatore: disponibilità

In modo analogo all’interfaccia “prenotazioni” del bambino, l’accompagnatore potrà indicare la propria disponibilità per le varie corse (così come per le prenotazioni, la disponibilità non può essere più cambiata dopo che l’amministratore ha “chiuso” i turni per quella corsa).

L’accompagnatore è di default disponibile dalla prima fermata della corsa all’andata e dalla scuola al ritorno, ma anche lui può essere assegnato ad una fermata differente.

In tale interfaccia, dopo la definizione dei turni per ciascuna corsa, l’accompagnatore potrà verificare se la propria disponibilità è stata utilizzata o no (cioè se è stato assegnato a quel turno), ed eventualmente confermare la presa visione.

### Interfaccia amministratore: turni

In modo analogo all’interfaccia “presenze” l’amministratore potrà confermare per ogni corsa le disponibilità di uno o più accompagnatori assegnandogli il turno su quella corsa. L’interfaccia dei turni dovrebbe avere un pulsante “consolida turno” per ogni corsa del pedibus (o dei checkbox per selezionare le corse e un pulsante generale per consolidare i turni delle corse selezionate) tramite cui finalizzare il turno e inviare una comunicazione agli accompagnatori. (ovviamente il turno può essere riaperto in caso siano necessarie modifiche).

Sempre tramite l’interfaccia turni, l’amministratore può trovare riscontro delle conferme degli
accompagnatori a cui sono stati affidate le corse del pedibus.

### Interfaccia: comunicazioni

In una apposita vista l’utente potrà visualizzare le comunicazioni a lui indirizzate. Ad esempio:

  1. Per l’amministratore, quando un accompagnatore ha confermato un turno;
  2. Per un accompagnatore quando l’amministrazione ha consolidato il turno di una corsa del pedibus;
  3. Per gli accompagnatori quando un bambino indica la sua presenza (o la rimuove) da una corsa del pedibus.

In ogni vista dell’applicazione si predisponga una modalità con cui avvertire l’utente che è arrivata una nuova comunicazione o che ci sono N comunicazioni ancora da leggere dall’ultima volta che si è consultata la vista delle comunicazioni.

## Run

L'applicazione è costituita da un backend scritto in Java con tecnologia Spring ed un frontend scritto in TypeScript utilizzando il framework Angular.

L'applicazione backend fa uso di un database PostgreSQL ed offre delle API REST per la gestione dei dati mostrati sopra, interrogate dall'applicazione frontend.

### Database

Per inizializzare il database, è necessario eseguire lo script `pedibus.sql` con il comando

```bash
> psql -U postgres -d pedibus < pedibus.sql
```

> Il comando `psql -U postgres -d pedibus < pedibus.sql` potrebbe non funzionare in ambiente Windows a causa dell'operatore `<`. In questo caso, utilizzare il comando `Get-Content pedibus.sql | psql -U postgres -d pedibus`.

Lo script contiene i dati di alcuni utenti:

| Email | Password | Ruolo |
| ------- | -------- | ----- |
| superuser@mail.it | SuperUser00 | Super amministratore |
| mario.rossi@mail.it | mario_rossi | Amministratore |
| luigi.verdi@mail.it | luigi_verdi | Amministratore |
| giovanni.bianchi@mail.it | giovanni_bianchi | Amministratore |
| john.doe@mail.it | JohnDoe11 | Utente |
| jane.doe@mail.it | JaneDoe44 | Utente |

#### Clean up

In alcuni casi, potrebbe essere necessario effettuare una pulizia del database e ripartire da una configurazione pulita.

1. Connettersi al database `pedibus` con l'utente `postgres`:

   ```bash
   > psql -U postgres -d pedibus
   ```

1. Eliminare completamente lo schema `public`:

   ```bash
   > DROP SCHEMA public CASCADE;
   ```

1. Ricreare lo schema `public`:
  
   ```bash
   > CREATE SCHEMA public;
   ```

### Backend

Per compilare l'applicazione frontend spostarsi nella directory `spring` ed eseguire il comando

```bash
> .\mvnw package
```

> È necessario avere impostato correttamente la variabile d'ambiente `JAVA_HOME`.

### Frontend

Per compilare l'applicazione frontend spostarsi nella directory `frontend` ed eseguire il comando

```bash
> ng build
```

### Docker Compose

Per eseguire l'applicazione Pedibus lanciare il comando

```bash
> docker-compose up -d
```

> L'applicazione sarà disponibile sulla porta `80` del sistema.

### DockerHub

È anche possibile eseguire l'applicazione direttamente da Docker senza installare alcuna dipendenza sul proprio sistema:

1. Creare la rete `pedibus`:

   ```bash
   > docker create network pedibus
   ```

1. Avviare il servizio di mail:

   ```bash
   > docker run --network pedibus --name mail -p 8025:8025 -d mailhog/mailhog
   ```

1. Avviare il database:

   ```bash
   > docker run --network pedibus --name postgres -p 1234:5432 -d francoe/pedibus_postgres
   ```

1. Avviare il backend:

   ```bash
   > docker run --network pedibus --name spring -p 8080:8080 -e SPRING_DATASOURCE_URL=jdbc:postgresql://postgres:5432/pedibus -e SPRING_MAIL_HOST=mail -e SPRING_MAIL_PORT=1025 -d francoe/pedibus_backend
   ```

1. Avviare il frontend:

   ```bash
   > docker run --network pedibus --name angular -p 4200:80 -d francoe/pedibus_frontend
   ```

1. L'applicazione sarà disponibile sulla porta `4200`, indicata nell'ultimo comando.

### Documentazione API

La documentazione delle API, generata con Postman è disponibile a questo [indirizzo](https://documenter.getpostman.com/view/11906497/T17FAoaw).

## Note

  1. Ovviamente l’accesso alle varie funzionalità deve essere verificato e autorizzato in funzione dei ruoli di ciascun utente;
  2. Ciascuna interfaccia, se visualizza dati che possono essere contemporaneamente modificati da altri utenti, deve essere in grado di aggiornarsi in tempo reale senza necessitare di un ricaricamento della stessa esplicito da parte dell’utente.

## Interfaccia REST

Il server deve esporre le funzionalità offerte al client attraverso una interfaccia REST utilizzabile anche da applicazioni terze (es. dall’applicazione mobile).

## Valutazione

La valutazione terrà conto dei seguenti aspetti:

- Soddisfacimento delle specifiche e correttezza del funzionamento del sistema e gestione delle varie funzionalità;
- Usabilità e funzionalità dell’interfaccia utente;
- Ingegnerizzazione e scalabilità della implementazione proposta;
- Leggibilità del codice e documentazione tramite commenti;
- Originalità ed efficacia nel raggiungimento degli obiettivi del progetto.

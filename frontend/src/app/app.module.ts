import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MatMomentDateModule } from "@angular/material-moment-adapter";
import {
  MatButtonModule,
  MatCardModule,
  MatListModule,
  MatTabsModule,
  MatIconModule,
  MatToolbarModule,
  MatInputModule,
  MatSidenavModule,
  MatSelectModule,
  MatCheckboxModule,
  MatDatepicker,
  MatDatepickerModule,
  MatNativeDateModule,
  MatTableModule, MatButtonToggleModule, MatBadgeModule,

} from '@angular/material';
import { LoginComponent } from './login/login.component';
import { Routes, RouterModule } from '@angular/router';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RegistrationComponent } from './registration/registration.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NavbarComponent } from './navbar/navbar.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthInterceptor } from './_interceptors/auth.interceptor';
import { AuthGuard } from './_guards/auth.guard';
import { ReservationComponent } from './reservation/reservation.component';
import { PasswordMatchValidatorDirective } from './_shared/password-match-validator.directive';
import { Role } from './data/role';
import { HomeComponent } from './home/home.component';
import { UserBookingComponent } from './user-booking/user-booking.component';
import { SelectAdminComponent } from './select-admin/select-admin.component';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';
import { ParentAvailabilityComponent } from './parent-availability/parent-availability.component';
import { SelectCompanionComponent } from './select-companion/select-companion.component';
import { MessagesComponent } from './messages/messages.component';
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegistrationComponent },
  {
    path: 'reservation',
    component: ReservationComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.Admin, Role.SystemAdmin] }
  },
  {path: 'home', component: HomeComponent},
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  { path: 'user-booking', component: UserBookingComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.User, Role.Admin, Role.SystemAdmin] }
  },
  {
    path: 'select-admin', component: SelectAdminComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.Admin, Role.SystemAdmin] }
  },
  {
    path: 'parent-availability', component: ParentAvailabilityComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.User, Role.Admin, Role.SystemAdmin] }
  },
  {
    path: 'select-companion', component: SelectCompanionComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.Admin, Role.SystemAdmin] }
  },
  {
    path: 'messages', component: MessagesComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.User, Role.Admin] }
  },
  {path: 'user-dashboard', component: UserDashboardComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.User, Role.Admin, Role.SystemAdmin] }

}];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    ReservationComponent,
    NavbarComponent,
    PasswordMatchValidatorDirective,
    HomeComponent,
    UserBookingComponent,
    UserDashboardComponent,
    SelectAdminComponent,
    ParentAvailabilityComponent,
    SelectCompanionComponent,
    MessagesComponent
  ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatListModule,
        MatCardModule,
        MatTabsModule,
        MatIconModule,
        MatToolbarModule,
        RouterModule.forRoot(routes),//, {enableTracing: true}),
        MatFormFieldModule,
        ReactiveFormsModule,
        MatInputModule,
        MatCheckboxModule,
        MatSidenavModule,
        FlexLayoutModule,
        HttpClientModule,
        MatSelectModule,
        FormsModule,
        MatDatepickerModule,
        MatMomentDateModule,
        MatNativeDateModule,
        MatTableModule,
        MatButtonToggleModule,
        MatBadgeModule,
        MatSlideToggleModule
    ],
  exports: [
    MatDatepickerModule,
    MatNativeDateModule,
    RouterModule
  ],
  providers: [
    AuthGuard,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

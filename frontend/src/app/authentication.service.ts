import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {catchError, map, delay} from 'rxjs/operators';
import {LoginResponse} from './data/login-response';
import * as moment from 'moment';
import {RegistrationRequest} from './data/registration-request';
import {Observable, throwError, BehaviorSubject} from 'rxjs';
import { Child } from './data/child';
import { Variable } from '@angular/compiler/src/render3/r3_ast';
import { Router } from '@angular/router';
import { UserVM } from './data/userVM';
import { environment } from './../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  

  private currentUserSubject: BehaviorSubject<LoginResponse>;
  private currentUser: Observable<LoginResponse>;
  private options;
  private sessionData: LoginResponse;
  private user$: Observable<UserVM>;
  private id: number;

  constructor(private http: HttpClient, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<LoginResponse>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
    
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'});
    this.options = { headers: headers };
  }

  public get currentUserValue(): LoginResponse {
    return this.currentUserSubject.value;
  }

  public login(email: string, password: string): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(environment.SERVER + 'auth/login', {email, password})
      .pipe(map(loginResponse => {
        if (loginResponse && loginResponse.token) {
          localStorage.setItem('currentUser', JSON.stringify(loginResponse));
          this.currentUserSubject.next(loginResponse);
        }
        console.log("Login Response: " + loginResponse.email);
        console.log("Id: " + loginResponse.id + " - Email: " + loginResponse.email + " - Token: " + loginResponse.token + " - Role: " + loginResponse.role);   
        this.id = loginResponse.id;
        console.log("saved id: " + this.id);

        return loginResponse;
      }));
  }

  public registration(firstName: string, lastName: string, email: string, password: string, passwordConfirm: string, children: Child[]) {
    console.log('CHILDREN:\n');
    console.log(children);
    return this.http.post<RegistrationRequest>(environment.SERVER + 'auth/register', {
      email,
      firstName,
      lastName,
      password,
      passwordConfirm,
      children
    }, this.options).pipe(catchError(this.handleError));

  }

  /*
   * Remove JWT from local storage
  */
  public logout(): void {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
    this.router.navigate(['/home']);
  }
  private isLoggedIn(): boolean {
    this.sessionData = JSON.parse(localStorage.getItem('currentUser'));      
    return this.sessionData === (undefined || null) ? false : true;
  }

  public isLoggedOut(): boolean {
    return !this.isLoggedIn();
  }

  private getExpiration() {
    return moment(this.currentUserValue.expiresAt);
  }

  isAuthenticated() {
    return this.isLoggedIn();
  }

  private isAdmin(): boolean {
    if(this.currentUserSubject.value.role === 'ROLE_ADMIN' || this.currentUserSubject.value.role === 'ROLE_SUPER_ADMIN')
      return true;
    else 
      return false;
  }

  private handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
  
}




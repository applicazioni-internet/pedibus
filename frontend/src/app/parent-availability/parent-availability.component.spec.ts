import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParentAvailabilityComponent } from './parent-availability.component';

describe('ParentAvailabilityComponent', () => {
  let component: ParentAvailabilityComponent;
  let fixture: ComponentFixture<ParentAvailabilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentAvailabilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParentAvailabilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

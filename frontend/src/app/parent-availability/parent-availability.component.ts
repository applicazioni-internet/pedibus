import { Component, OnInit } from '@angular/core';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {Line} from '../data/line';
import {BehaviorSubject, Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {ChildReservation} from '../data/child-reservation';
import {MatTableDataSource} from '@angular/material';
import {SelectElement} from '../select-admin/select-admin.component';
import {SelectionModel} from '@angular/cdk/collections';
import {DataService} from "../data.service";
import {formatDate} from "@angular/common";
import {UserVM} from "../data/userVM";
import {LoginResponse} from "../data/login-response";
import {Availability} from "../data/availability";
import {FormControl, FormGroup} from "@angular/forms";


export interface LineForDate {
  line: number[];
  date: Date;

}

@Component({
  selector: 'app-parent-availability',
  templateUrl: './parent-availability.component.html',
  styleUrls: ['./parent-availability.component.css']
})

export class ParentAvailabilityComponent implements OnInit {
  private lines$: Observable<Line[]>;
  private lines: Line[];
  private bookingForm: FormGroup;
  private availabilities$: Observable<Availability[]>;
  private availabilities: Availability[];

  private sessionData: LoginResponse;
  private line: Line;
  date = new FormControl(new Date());
  private dateAvailabilities: Date;
  private dateString: string;
  private selected: LineForDate;
  private currentLineIndex = new BehaviorSubject<number>(0);
  private currentLine$: Observable<Line>;


  /*tmp: Line[] = [{name: 'linea1', administrators: ['Matteo', 'Gianni'], forward: [], backward: []},
    {name: 'linea2', administrators: ['Franco', 'Mario'], forward: [], backward: []}
  ];*/
  events: string;
  displayedColumns: string[] = ['select', 'name'];
  dataSource = new MatTableDataSource<SelectElement>();
  selection = new SelectionModel<SelectElement>(true, []);
  constructor(private dataService: DataService) {
    this.dateAvailabilities  = new Date();
    this.loadData();
  }

  ngOnInit() {
  }

  loadData() {
    //  this.lines$ = this.dataService.getLines();
    //this.lines$ = Observable.of(this.tmp);
    this.sessionData = JSON.parse(localStorage.getItem('currentUser'));
    console.log("[USER-BOOKING] => Id: "+ this.sessionData.id + " - Email: " + this.sessionData.email);
    this.lines$ = this.dataService.getLines();
    this.lines$.subscribe(res => {
      this.lines = res;
    });
    this.currentLineIndex.asObservable().subscribe(index => {
      this.currentLine$ = this.lines$.pipe(map(list => list[index]));
      this.currentLine$.subscribe(res => {
        this.line = res;
      });
    });
    this.availabilities$ = this.dataService.getUserAvailability(this.sessionData.id, formatDate(this.dateAvailabilities, 'yyyy-MM-dd', 'en'));
    this.availabilities$.subscribe(res => {
        console.log("REESS");
        console.log(res);
        this.availabilities = res;
    });
  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.dataSource = new MatTableDataSource<SelectElement>();
    this.dateString = `${event.value.toDateString()}`;

  }

  addAvailability() {
    console.log(this.line);
    console.log(this.dateString);
    this.dataService.postNewAvailability({lineId: this.line.id, date: formatDate(this.dateString,  'yyyy-MM-dd', 'en') }).subscribe(res =>{
      this.loadData();
    });
  }


  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: SelectElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  backDate() {
    console.log(this.dateAvailabilities.getDate());

    this.dateAvailabilities = new Date(this.dateAvailabilities.setDate(this.dateAvailabilities.getDate() - 1));
    console.log(this.dateAvailabilities.getDate());

    this.loadData();
    console.log(formatDate(this.dateAvailabilities, 'yyyy/MM/dd', 'en'));
  }

  nextDate() {
    console.log(this.dateAvailabilities.getDate());
    this.dateAvailabilities = new Date(this.dateAvailabilities.setDate(this.dateAvailabilities.getDate() + 1));
    console.log(this.dateAvailabilities.getDate());

    this.loadData();
    console.log(formatDate(this.dateAvailabilities, 'yyyy/MM/dd', 'en'));
  }
  clickDeleteAvailability(id: number) {
    console.log(id);
    this.dataService.deleteAvailability(id).subscribe(() =>  this.loadData());
  }
}

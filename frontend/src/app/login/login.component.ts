import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private loginForm: FormGroup;
  validationMessages: any;
  private errMsg: boolean = false;

  constructor(private fb: FormBuilder, private authService: AuthenticationService, private router: Router) {
    this.validationMessages = {
      email: [
        { type: 'required', message: 'Email is required' },
        { type: 'email', message: 'Email must be a valid address' },
        { type: 'minlength', message: 'Email too short' },
        { type: 'maxlength', message: 'Email too long' }
      ],
      password: [
        { type: 'required', message: 'Password is required' },
        { type: 'maxlength', message: 'Password too long' }
      ]
    };
  }

  ngOnInit() {
    this.generateFormGroup();
  }

  private generateFormGroup() {
    this.loginForm = this.fb.group({
      email: [null, [
        Validators.required,
        Validators.email,
        Validators.minLength(6),
        Validators.maxLength(64)
      ]],
      password: [null, [
        Validators.required,
        // TODO Reinsert?
        // Validators.pattern(/(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])/),
        // Validators.minLength(6),
        Validators.maxLength(64)
      ]]
    });
  }

  /*
  * This method receives the form group value
  */
  submit(validateForm: any): void {
    console.log(validateForm);
    // TODO Manage code
    this.authService.login(validateForm.email, validateForm.password)
      .subscribe(
        response => {
          console.log(response);
          console.log("Login Successful -> Going to Reservation Page");
          this.errMsg = false;
          this.router.navigate(['/reservation']);
        },
        err => {
          validateForm
          console.error(err.message);
          this.errMsg = true;
        },
        () => {
          console.log('completed');
        }
      );
  }
}

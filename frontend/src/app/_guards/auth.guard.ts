import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CanActivate } from '@angular/router';
import { AuthenticationService } from '../authentication.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private authenticationService: AuthenticationService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser = this.authenticationService.currentUserValue;

    if (currentUser) {
      if (route.data.roles && route.data.roles.indexOf(currentUser.role) === -1) {
        // Not authorized
        console.log("User Not Authorized");        
        this.router.navigate(['/']);
        return false;
      }

      // Authorized
      console.log("User Authorized");
      return true;
    }

    // Not logged
    this.router.navigate(['/login']); // { queryParams: { returnUrl: state.url }});
    return false;
  }
}

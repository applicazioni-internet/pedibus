import {Component} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {AuthenticationService} from '../authentication.service';
import {Line} from '../data/line';
import {DataService} from '../data.service';
import {LineReservations, ChildInfo} from '../data/line-reservations';
import {Reservation} from '../data/reservation';
import {formatDate} from '@angular/common';
import {Stop} from '../data/stop';
import {Direction} from '../data/direction';
import {filter, map, tap} from 'rxjs/operators';
import {BehaviorSubject, Observable} from 'rxjs';
import {UserVM} from '../data/userVM';
import { ChildReservation } from '../data/child-reservation';
import { Child } from '../data/child';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})
export class ReservationComponent {
  private lines$: Observable<Line[]>;
  private date: Date;
  private currentLine$: Observable<Line>;
  private reservations$: Observable<LineReservations>;
  private forwardUsers$: Observable<UserVM[]>;
  private backwardUsers$: Observable<UserVM[]>;
  private currentLineIndex = new BehaviorSubject<number>(0);
  private stopSelected: string;

  private backward: Stop[];
  private forward: Stop[];
  private setting = {
    element: {
      dynamicDownload: null as HTMLElement
    }
  }

  constructor(private fb: FormBuilder, private authService: AuthenticationService, private dataService: DataService, private router: Router) {
    //Init Data
    //this.forwardReservationsP = [];
    
    this.date = new Date();
    this.loadData();
  }

  loadData() {
    this.lines$ = this.dataService.getLines();
    /*this.currentLineIndex.asObservable().subscribe(index => {
      this.currentLine$ = this.lines$.pipe(map(list => list[index]), tap(line => this.loadLineData(line, index)));
    });*/

    //Get Lines Data
    this.lines$ = this.dataService.getLines();
    this.currentLineIndex.asObservable().subscribe(index => {
      this.currentLine$ = this.lines$.pipe(map(list => list[index]));
      this.currentLine$.subscribe(line => {
        //Get Line Forward Stops
        this.forward = line.forward;
        line.forward.forEach(stop => {
          //console.log("Stop " + stop.name);
          
        });

        //Get Line Backward Stops
        this.backward = line.backward;
        line.backward.forEach(stop => {
          //console.log("Stop " + stop.name);
                    
        });
        
      });

    }); 

    this.currentLine$.subscribe(line => {
        //LOAD RESERVATIONS
       this.loadLineReservation(line.id); 
    }); 
    
  }

  loadLineReservation(lineId: number) {
    // Upload Line Reservations
    this.reservations$ = this.dataService.getLinesReservations(lineId, this.date);
    this.reservations$.subscribe(reservations => {
      console.log("==== Got Line Reservations ====");
  
      console.log("=== FORWARD RESERVATIONS ===");
      reservations.forward.forEach(forwardRes => {
        console.log("-- " + forwardRes.id + " --");
        console.log("======== PRESENT CHILD ========");
        forwardRes.present.forEach(present => {
          console.log("-------- " + present.child + " --------");
        });    
      });

      console.log("=== BACKWARD RESERVATIONS ===");
      reservations.backward.forEach(backwardRes => {
        console.log("-- " + backwardRes.id + " --");        
      });
    });
  }

  dynamicDownloadJson() {
    this.reservations$.subscribe((res) => {
      this.dyanmicDownloadByHtmlTag({
        fileName: 'reservations.json',
        text: JSON.stringify(res)
      });
    });
  }
  
  

  private dyanmicDownloadByHtmlTag(arg: {
    fileName: string,
    text: string
  }) {
    if (!this.setting.element.dynamicDownload) {
      this.setting.element.dynamicDownload = document.createElement('a');
    }
    const element = this.setting.element.dynamicDownload;
    const fileType = arg.fileName.indexOf('.json') > -1 ? 'text/json' : 'text/plain';
    element.setAttribute('href', `data:${fileType};charset=utf-8,${encodeURIComponent(arg.text)}`);
    element.setAttribute('download', arg.fileName);

    var event = new MouseEvent("click");
    element.dispatchEvent(event);
  }

  areStopEqual(stop1: Stop, stop2: Stop){
    if(stop1.id == stop2.id)
      return true;
    else
      return false;
  }

  getForwardStopReservations(reservations: LineReservations, stop: number, presence: boolean) {
    console.log("Received Request for Line: " + this.currentLineIndex.value  + " - Stop " + stop);
    return this.getStopReservations(reservations, stop, Direction.FORWARD, presence);
  }

  getBackwardStopReservations(reservations: LineReservations, stop: number, presence: boolean) {
    return this.getStopReservations(reservations, stop, Direction.BACKWARD, presence);
  }

  getStopReservations(reservations: LineReservations, stop: number, direction: Direction, presence: boolean) {
    if(presence==true){ // Return Present
      if( direction === Direction.FORWARD ) { // FORWARD        
          if(reservations.forward.find(res => res.id === stop) == (undefined || null)){
              return null;
          } else {
              return reservations.forward.find(res => res.id === stop).present;
          }
      } else{ // BACKWARD
          if(reservations.backward.find(res => res.id === stop) == (undefined || null)){
              return null;
          } else {
              return reservations.backward.find(res => res.id === stop).present;
          }
      }
    
    } else { // Return Absent
        if( direction === Direction.FORWARD ) { // FORWARD        
            if(reservations.forward.find(res => res.id === stop) == (undefined || null)){
                return null;
            } else {
                return reservations.forward.find(res => res.id === stop).absent;
            }
        } else{ // BACKWARD
            if(reservations.backward.find(res => res.id === stop) == (undefined || null)){
                return null;
            } else {
                return reservations.backward.find(res => res.id === stop).absent;
            }
        }     
    }
  }

  toggleStudent(reservationId: number): void {
    this.dataService.updateReservation(reservationId).subscribe(() => this.loadData());
    this.router.navigate(['/reservation']);
  }

  backDate() {
    this.date = new Date(this.date.setDate(this.date.getDate() - 1));
    this.loadData();
    console.log(formatDate(this.date, 'yyyy/MM/dd', 'en'));
  }

  nextDate() {
    this.date = new Date(this.date.setDate(this.date.getDate() + 1));
    this.loadData();
    console.log(formatDate(this.date, 'yyyy/MM/dd', 'en'));
  }

}

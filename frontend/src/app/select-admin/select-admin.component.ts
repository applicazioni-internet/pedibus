import {Component} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Line} from '../data/line';
import {FormBuilder} from '@angular/forms';
import {AuthenticationService} from '../authentication.service';
import {DataService} from '../data.service';
import 'rxjs/add/observable/of';
import {MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {UserVM} from "../data/userVM";
import {Role} from "../data/role";


export interface SelectElement {
  name: string;
  position: number;

}


@Component({
  selector: 'app-select-admin',
  templateUrl: './select-admin.component.html',
  styleUrls: ['./select-admin.component.css']
})
export class SelectAdminComponent {
  private lines$: Observable<Line[]>;
  private currentLine$: Observable<Line>;
  private userSelected: number;
  private users$: Observable<UserVM[]>;
  private currentLineIndex = new BehaviorSubject<number>(0);
  displayedColumns: string[] = ['select', 'name'];
  dataSource = new MatTableDataSource<SelectElement>();
  selection = new SelectionModel<SelectElement>(true, []);
  events: string;
  /*tmp: Line[] = [{name: 'linea1', administrators: ['Matteo', 'Gianni'], forward: [], backward: []},
                 {name: 'linea2', administrators: ['Franco', 'Mario'], forward: [], backward: []}
  ];*/

  constructor(private fb: FormBuilder, private authService: AuthenticationService, private dataService: DataService) {
    this.loadData();

  }

  loadData() {
      this.users$ = this.dataService.getAllUser();
      this.users$.subscribe(res => {
        console.log("USERS");
        console.log(res);
      });
  }
  isAdmin(event: Role) {
    console.log('Role: ');
    if (event.valueOf() === Role.Admin.valueOf() ||  event.valueOf() === Role.SystemAdmin.valueOf()) {
      return true;
    }
    return false;
  }
  changed(id: number, email: string, event: Role) {
    console.log('Role: ' + event);

    if (event.valueOf() === Role.Admin.valueOf() ||  event.valueOf() === Role.SystemAdmin.valueOf()) {
      this.dataService.changeRoleUser(id, { promote: false }).subscribe(() => console.log("role Admin"));
      this.dataService.insertMessage({body: "you are no longer an admin. !!", recipient: email}).subscribe(res => {
        console.log(res);
      });
    } else {
      this.dataService.changeRoleUser(id, {promote: true}).subscribe(() => console.log("role Not Admin"));
      this.dataService.insertMessage({body: "You are now admin !!", recipient: email}).subscribe(res => {
        console.log(res);
      });
    }
    this.loadData();
  }



  /** Whether the number of selected elements matches the total number of rows. */


  /** Selects all rows if they are not all selected; otherwise clear selection. */


  /** The label for the checkbox on the passed row */



}


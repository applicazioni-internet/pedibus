import { Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {AuthenticationService} from '../authentication.service';
import {Line} from '../data/line';
import {DataService} from '../data.service';
import {LineReservations} from '../data/line-reservations';
import {Reservation} from '../data/reservation';
import {formatDate} from '@angular/common';
import {Stop} from '../data/stop';
import {Direction} from '../data/direction';
import {filter, map, tap} from 'rxjs/operators';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {UserVM} from '../data/userVM';
import { Child } from '../data/child';
import {AbstractControl} from '@angular/forms';
import {LoginResponse} from '../data/login-response';
import * as moment from 'moment';
import { Moment } from 'moment';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { IfStmt } from '@angular/compiler';

@Component({
  selector: 'app-user-booking',
  templateUrl: './user-booking.component.html',
  styleUrls: ['./user-booking.component.css']
})
export class UserBookingComponent implements OnInit{
  private lines$: Observable<Line[]>;
  private date: Date;
  private currentLine$: Observable<Line>;
  private reservations$: Observable<LineReservations>;
  private forwardUsers$: Observable<UserVM[]>;
  private backwardUsers$: Observable<UserVM[]>;
  private currentLineIndex = new BehaviorSubject<number>(0);
  private backward: Stop[];
  private forward: Stop[];
  private sessionData: LoginResponse;
  private stopSelected: string;
  private userSelected: number;
  private childSelected: number;
  private currentUser$: Observable<UserVM>;
  private childs: Child[];
  private bookingForm: FormGroup;
  private buttonDisabled: boolean;

  private stopArrival: Stop;
  private stopDeparture: Stop;
  router: any;
                      
  constructor(private fb: FormBuilder, private authService: AuthenticationService, private dataService: DataService) {
    this.date = new Date();
    this.buttonDisabled = true;
    this.stopArrival = {id: Number.MIN_SAFE_INTEGER, name: null, time: null, latitude: null, longitude: null, direction: null};
    this.stopDeparture = {id: Number.MAX_SAFE_INTEGER, name: null, time: null, latitude: null, longitude: null, direction: null};
    //Load All Data
    this.loadData();
  }

  loadData() {
    //Get Lines Data
    this.lines$ = this.dataService.getLines();
    this.currentLineIndex.asObservable().subscribe(index => {
      this.currentLine$ = this.lines$.pipe(map(list => list[index]));
      this.currentLine$.subscribe(line => {
        //Get Line Forward Stops
        this.forward = line.forward;
        line.forward.forEach(stop => {
          if(stop.id > this.stopArrival.id){
              this.stopArrival = stop;
              console.log("New Stop Arrival: " + this.stopArrival.id);
          }
        });

        //Get Line Backward Stops
        this.backward = line.backward;
        line.backward.forEach(stop => {
          if(stop.id < this.stopDeparture.id){
              this.stopDeparture = stop;
              console.log("New Stop Departure: " + this.stopDeparture.id);
          }

          //console.log("Stop " + stop.name);         
        });
      });
      //, tap(line => this.loadLineData(line, index)));
    });
    
    //Load Cookie Data - To GET current User Data
    this.sessionData = JSON.parse(localStorage.getItem('currentUser'));
    console.log("[USER-BOOKING] => Id: "+ this.sessionData.id + " - Email: " + this.sessionData.email);
    
    //Get Current User Data
    this.currentUser$ = this.dataService.getUserData(this.sessionData.id);
    this.currentUser$.subscribe(user => {
      console.log("User Got => Name: " + user.firstName + " - LastName: " + user.lastName);
      console.log("Childs Got => " );
      
      //Get Childs
      this.childs = user.children;
      user.children.forEach(child => {
        console.log("== Child " + child.id + " ==");        
        console.log(child.firstName + " " + child.lastName + " " + child.birthDate); 
        if(child.reservations != null){
          //Get Child Reservations
          child.reservations.forEach(res => { 
            console.log("Reservation Id: " + res.id) 
          });
        }
            
      });
     
    });
    
  }


  ngOnInit() {
    this.generateFormGroup();
  }

  private generateFormGroup() {
    this.bookingForm = this.fb.group({
      child: [null, [
        Validators.required]],
      stopDeparture: [null, [
        //Validators.required
      ]],
      stopArrival: [null, [
        //Validators.required
      ]],
      date: [null, [
        Validators.required,
        DateValidator.dateGtToday
      ]]
    });
  }

  backDate() {
    this.date = new Date(this.date.setDate(this.date.getDate() - 1));
    if(this.date <= new Date() && this.buttonDisabled==false)
      this.buttonDisabled = true;
    this.loadData();
    console.log("Disabled: " + this.buttonDisabled + " - " + formatDate(this.date, 'yyyy/MM/dd', 'en'));
  }

  nextDate() {
    this.date = new Date(this.date.setDate(this.date.getDate() + 1));
    if(this.date > new Date() && this.buttonDisabled==true) 
      this.buttonDisabled = false;
    this.loadData();
    console.log("Disabled: " + this.buttonDisabled + " - " + formatDate(this.date, 'yyyy/MM/dd', 'en'));
  }

  insertReservation(line: number, directions: string, currentLine: Line, validateForm: any ){
    console.log("Line: " + line + " - Direction: " + directions + " - CurrentLineId: " + currentLine.id);
    console.log("=== BOOKING FORM ===");
    console.log("Line: " + currentLine.id + " - Reservation Date: " + formatDate(this.bookingForm.value.date, 'yyyy-MM-dd', 'en'));    
    console.log(this.bookingForm.value);
    
    if(directions == "FORWARD"){
   
      console.log("==== [F] Arrival Stop: " + this.stopArrival.id + "====");
      this.dataService.postNewReservation(line, this.bookingForm.value.date, {
        departureId: this.bookingForm.value.stopDeparture, 
        arrivalId: this.stopArrival.id, 
        presence: false, 
        childId: this.bookingForm.value.child
      }).subscribe(() => {
        this.loadData();
      });

    }else {
      
      console.log("==== [B] Departure Stop: " + this.stopDeparture.id + "====");
      this.dataService.postNewReservation(line, this.bookingForm.value.date, {
        departureId: this.stopDeparture.id, 
        arrivalId: this.bookingForm.value.stopArrival,
        presence: false, 
        childId: this.bookingForm.value.child
      }).subscribe(() => {
        this.loadData();
      });

    } 

  }

}

export class DateValidator {
  static dateGtToday(AC: AbstractControl) {
    //if (AC && AC.value && moment().isAfter(new Date())) {
      if (AC.value < new Date()) {
      return {'dateValidator': true};
    }
    return null;
  }
}


export interface RegistrationRequest {
  email: string;
  firstName: string;
  lastName: string;
  password: string;
  passwordConfirm: string;
}

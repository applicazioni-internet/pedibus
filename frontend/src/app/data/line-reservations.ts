export interface LineReservations {
  forward: StopReservation[];
  backward: StopReservation[];
}

export interface StopReservation {
  id: number; //stopId
  present: ChildInfo[];
  absent: ChildInfo[];
}

export interface ChildInfo {
  child: number;
  reservation: number; 
}

/*export interface LineReservations {
  forward: StopReservation[];
  backward: StopReservation[];
}

export interface StopReservation {
  stop: string;
  presentStudents: {
    id: number;
    student: string;
  }[];
  absentStudents: {
    id: number;
    student: string;
  }[];
}*/

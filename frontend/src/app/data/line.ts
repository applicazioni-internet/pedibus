import { Stop } from './stop';
import { User } from './user';

export interface Line {
  id: number;
  name: string;
  administrators: User[];
  companions: User[];
  forward: Stop[];
  backward: Stop[];
}
export interface User {
  id: number;
  firstName: string;
  lastName: string;
  enabled: boolean;
  email: string;
}

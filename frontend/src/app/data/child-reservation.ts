import { Line } from './line';
import { Stop } from './stop';
import { Direction } from '@angular/cdk/bidi';

export interface ChildReservation {
    id: number;
    line: number;
    departure: Stop;
    arrival: Stop;
    presence: boolean;
    date: Date;
}
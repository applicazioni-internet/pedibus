
export interface Message {
  id: number;
  body: string;
  recipient: string;
  sender: string;
  date: Date;
  read: boolean;
}

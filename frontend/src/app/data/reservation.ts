import { User } from './user';
import { Stop } from './stop';
import { Child } from './child';
import { Line } from './line';

export interface Reservation {
    id: number;
    date: Date; 
    child: Child;
    line: Line;
    departure: Stop;
    arrival: Stop;
    presence: boolean;
}

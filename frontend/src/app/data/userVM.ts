import {Role} from './role';
import { Child } from './child';
import { Line } from './line';

export interface UserVM {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  enabled: boolean;
  companionLines: Line[];
  children: Child[];
  role: Role;

}

import { Role } from './role';

export interface LoginResponse {
    id: number;
    email: string;
    expiresAt: Date;
    role: Role;
    token: string;
}

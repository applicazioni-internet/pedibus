import { ChildReservation } from './child-reservation';

export interface Child {
    id: number;
    firstName: string;
    lastName: string;
    birthDate: Date;
    reservations: ChildReservation[];
}
  
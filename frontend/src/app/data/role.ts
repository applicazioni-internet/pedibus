export enum Role {
    User = 'ROLE_USER',
    Admin = 'ROLE_ADMIN',
    SystemAdmin = 'ROLE_SUPER_ADMIN'
}

import { Time } from '@angular/common';

export interface Stop {
  id: number;
  name: string;
  time: Time;
  latitude: number;
  longitude: number;
  direction: number; //enum Direction Type
}

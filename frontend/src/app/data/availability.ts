export interface Availability {
  id: number;
  line: number;
  date: Date;
  user: string;
  approved: boolean;
}

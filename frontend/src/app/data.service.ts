import {Injectable} from '@angular/core';
import {Line} from './data/line';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Direction} from './data/direction';
import {map} from 'rxjs/operators';
import {LineReservations} from './data/line-reservations';
import {UserVM} from './data/userVM';
import {formatDate} from '@angular/common';
import { Child } from './data/child';
import { ChildReservations } from './data/child-reservations';
import { ChildReservation } from './data/child-reservation';
import { Reservation } from './data/reservation';
import {Message} from './data/message';
import {Availability} from './data/availability';
import { environment } from './../environments/environment';

@Injectable({providedIn: 'root'})
export class DataService {


  constructor(public http: HttpClient) {
  }

  public getLines(): Observable<Line[]> {
    return this.http.get<GetLinesResponse>(environment.SERVER + 'lines')
      .pipe(map(res => res._embedded.lineVMList));
  }

  // GET LINE RESERVATION
  public getLinesReservations(line: number, date: Date): Observable<LineReservations> {
    return this.http.get<LineReservations>(environment.SERVER + `reservations/${line}/${formatDate(date, 'yyyy-MM-dd', 'en')}`);
  }

  //Get User Data By UserId
  public getUserData(id: number): Observable<UserVM> {
    return this.http.get<UserVM>(environment.SERVER + `users/${id}`);
  }

  public getAllUser(): Observable<UserVM[]> {
    return this.http.get<GetUserResponse>(environment.SERVER + 'users')
      .pipe(map(res => res._embedded.userVMList));
  }

  public updateReservation(reservationId: number) {
    return this.http.put(environment.SERVER + `reservations/${reservationId}`, null);
  }


  public postNewReservation(line: number,
    date: Date,
    body: { departureId: number, arrivalId: number, presence: boolean, childId: number}) {
  return this.http.post(environment.SERVER + `reservations/${line}/${formatDate(date, 'yyyy-MM-dd', 'en')}`, body);
  }

  //GET CHILDREN
  public getChildReservation(childId: number): Observable<ChildReservation[]> {
    return this.http.get<GetChildReservationsResponse>(environment.SERVER + `reservations/children/${childId}`)
      .pipe(map(res => res._embedded.reservationVMList));
  }

  //DELETE CHILDREN RESERVATION
  public deleteChildReservation(reservationId: number) {
    return this.http.delete(environment.SERVER + `reservations/${reservationId}`);
  }

  //GET CHILDREN
  public getAllChildren(userId: number): Observable<Child[]> {
    return this.http.get<GetChildResponse>(environment.SERVER + `users/${userId}/$children/`)
      .pipe(map(res => res._embedded.childVMList));
  }
  //AVAILABILITY
  public postNewAvailability(body: { lineId: number, date: string}) {
    console.log(body);
    return this.http.post(environment.SERVER + `availabilities`, body);
  }
  public getUserAvailability(id: number, date: string): Observable<Availability[]> {
    return this.http.get<Availability[]>(environment.SERVER + `availabilities/user/${id}/date/${date}`);
  }
  public getLineAvailability(idLine: number, date: string): Observable<Availability[]> {
    return this.http.get<Availability[]>(environment.SERVER + `availabilities/line/${idLine}/date/${date}`);
  }
  public deleteAvailability(id: number) {
    return this.http.delete(environment.SERVER + `availabilities/` + id);
  }
  public approveAvailability(id: number) {
    return this.http.put(environment.SERVER + `availabilities/${id}`, null );
  }
  //MESSAGE
  public getMessage(): Observable<Message[]> {
    return this.http.get<Message[]>(environment.SERVER + `messages/received`);
  }
  public updateMessageRead(id: number, body: {read: boolean}) {
    console.log('read id: ' + id);
    console.log('body: ' + body.read);
    return this.http.put(environment.SERVER + `messages/${id}`, body);
  }
  public insertMessage( message: {body: string, recipient: string}) {
    console.log('body: ' + message.body);
    return this.http.post(environment.SERVER + `messages`, message);
  }
  //ADMIN
  public changeRoleUser(id: number, body: {promote: boolean}){
   return this.http.put(environment.SERVER + `users/${id}`, body);
  }

}

interface GetMessagesResponse {
  _embedded: { messageVMList: Message[] };
}

interface GetLinesResponse {
  _embedded: { lineVMList: Line[] };
}
interface GetAvailabilityResponse {
  _embedded: { availabilityVMList: Availability[] };
}
interface GetUserResponse {
  _embedded: { userVMList: UserVM[] };
}

interface GetChildResponse {
  _embedded: { childVMList: Child[] };
}

interface GetChildReservationsResponse {
  _embedded: { reservationVMList: ChildReservation[] };
}

import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../authentication.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  private logged: boolean;

  constructor(private auth: AuthenticationService) { }

  ngOnInit() { 
    console.log("Navbar Log Check");    
    this.logged = this.auth.isAuthenticated();
    console.log("Log response :" + this.logged); 
  }

  isLogged(){
    return this.logged;
  }

  logout(): void {
    this.auth.logout();
    this.logged = false;
  }

}

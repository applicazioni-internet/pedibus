import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectCompanionComponent } from './select-companion.component';

describe('SelectCompanionComponent', () => {
  let component: SelectCompanionComponent;
  let fixture: ComponentFixture<SelectCompanionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectCompanionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectCompanionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit} from '@angular/core';
import {MatDatepickerInputEvent, MatTableDataSource} from '@angular/material';
import {SelectElement} from '../select-admin/select-admin.component';
import {BehaviorSubject, Observable} from 'rxjs';
import {Line} from '../data/line';
import {SelectionModel} from '@angular/cdk/collections';
import {LineForDate} from '../parent-availability/parent-availability.component';
import {FormBuilder, FormControl} from '@angular/forms';
import {AuthenticationService} from '../authentication.service';
import {DataService} from '../data.service';
import {map, tap} from 'rxjs/operators';
import {Availability} from "../data/availability";
import {formatDate} from "@angular/common";
import {User} from "../data/user";
import {UserVM} from "../data/userVM";

export interface CompanionForDate {
  date: Date;
  position: number;

}

@Component({
  selector: 'app-select-companion',
  templateUrl: './select-companion.component.html',
  styleUrls: ['./select-companion.component.css']
})
export class SelectCompanionComponent implements OnInit {
  private lines$: Observable<Line[]>;
  private lines: Line[];
  private availabilities$: Observable<Availability[]>;
  private  line: Line;
  private selected: LineForDate;
  private currentLine$: Observable<Line>;
  private dateAvailabilities: Date;
  date = new FormControl(new Date());



  /*tmp: Line[] = [{name: 'linea1', administrators: ['Matteo', 'Gianni'], forward: [], backward: []},
    {name: 'linea2', administrators: ['Franco', 'Mario'], forward: [], backward: []}
  ];*/
  private currentLineIndex = new BehaviorSubject<number>(0);
  events: string;
  displayedColumns: string[] = ['select', 'name'];
  dataSource = new MatTableDataSource<SelectElement>();
  selection = new SelectionModel<SelectElement>(true, []);

  constructor(private fb: FormBuilder, private authService: AuthenticationService, private dataService: DataService) {
    this.dateAvailabilities  = new Date();
    this.loadData();

  }

  ngOnInit() {
  }


  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.dataSource = new MatTableDataSource<SelectElement>();
    this.events = `${event.value.toDateString()}`;
    this.dateAvailabilities = event.value;
    console.log('date: ' + this.events);
    // TODO
   // this.loadLineData(this.selected.line);
    console.log(this.events);
    this.loadData();
  }

  loadData() {
    //  this.lines$ = this.dataService.getLines();
    //this.lines$ = Observable.of(this.tmp);
    this.lines$ = this.dataService.getLines();
    this.lines$.subscribe(res => {
      this.lines = res;
    });
    this.currentLineIndex.asObservable().subscribe(index => {
      this.currentLine$ = this.lines$.pipe(map(list => list[index]));
      this.currentLine$.subscribe(res => {
        this.line = res;
        this.availabilities$ = this.dataService.getLineAvailability(this.line.id, formatDate(this.dateAvailabilities, 'yyyy-MM-dd', 'en'));
        this.availabilities$.subscribe(res2 => {
          console.log("REESS");
          console.log(res2);
          //this.availabilities = res;
        });
      });
    });

  }

  loadLineData(line: Line) {
    // this.users = this.dataService.getLinesUsers(Line.name)
    this.dataSource = new MatTableDataSource<SelectElement>();
    let i = 0;
    for (const adm of line.administrators ) {
      //this.dataSource.data.push({position: i, name: adm });
      i++;
    }
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  deleteAvailability(availability: Availability) {
    let message;
    this.dataService.deleteAvailability(availability.id).subscribe(() =>{
      message = "Your availability for line " + availability.line + " in date "+ availability.date + "has been cancelled";
      this.dataService.insertMessage({body: message , recipient: availability.user}).subscribe(res => {
        console.log(res);
      });
    });
    this.loadData();
  }

  approvedAvailability(availability: Availability) {
    let message;
    this.dataService.approveAvailability(availability.id).subscribe(() => {
        message = "Now you are the companion for line " + availability.line + " in date "+ availability.date;
        this.dataService.insertMessage({body: message , recipient: availability.user}).subscribe(res => {
          console.log(res);
        });
      }
    );
    this.loadData();
  }
}

import { Component, OnInit } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Child } from '../data/child';
import { FormBuilder, FormGroup, FormArray, ValidatorFn, FormControl } from '@angular/forms';
import { AuthenticationService } from '../authentication.service';
import { DataService } from '../data.service';
import { map, tap } from 'rxjs/operators';
import { ChildReservations } from '../data/child-reservations';
import { ChildReservation } from '../data/child-reservation';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material';
import { of } from 'rxjs';
import { LoginResponse } from '../data/login-response';
import { UserVM } from '../data/userVM';
import { Router } from '@angular/router';


@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css']
})

export class UserDashboardComponent implements OnInit {

  form: FormGroup;

  private childs$: Observable<Child[]>;
  private currentChildIndex = new BehaviorSubject<number>(0);
  private currentChild$: Observable<Child>;
  private reservations$: Observable<ChildReservation[]>;

  private sessionData: LoginResponse;
  private currentUser$: Observable<UserVM>;

  private reservations: ChildReservation[];
  private childReservations$ : Observable<ChildReservation[]>;
    
  private displayedColumns: string[] = ['select', 'id', 'line', 'departure', 'arrival', 'date'];
  private dataSource; 
  private selection = new SelectionModel<ChildReservation>(true, []);


  constructor(private fb: FormBuilder, private authService: AuthenticationService, private dataService: DataService, private router: Router) { 
    //Load Cookie Data - To GET current User Data
    this.sessionData = JSON.parse(localStorage.getItem('currentUser'));
    console.log("[USER-SESSION] => Id: "+ this.sessionData.id + " - Email: " + this.sessionData.email);

    //Get Current User Data
    this.currentUser$ = this.dataService.getUserData(this.sessionData.id);

    this.loadData();
  }

  async loadData() {

    this.currentUser$.subscribe(user => {
      console.log("=== User Got - Name: " + user.firstName + " - LastName: " + user.lastName + " ===");

      this.childs$ = Observable.of(user.children);
    }
    );

    await delay(300);

    this.childs$.subscribe(childs =>{
      childs.forEach(c => {
        console.log("=== Child " + c.id + " " + c.firstName + " " + c.lastName + " ===");        
      })
    });

    this.currentChildIndex.asObservable().subscribe(index => {     
      this.currentChild$ = this.childs$.pipe(map(list => list[index]));//, tap(child => this.loadChildReservations(child, index)));
      this.currentChild$.subscribe( child => {

        console.log("====== Got Child "+ child.id + " ======");
        //Getting Child Reservations Data
        this.childReservations$ = this.dataService.getChildReservation(child.id);
        this.childReservations$.subscribe(reservations => {

          console.log("===== Observable Child Reservations =====");
          reservations.forEach(res =>{
            console.log("------- Child " + child.id + " Reservation  - Id: " + res.id + " -------");       
          })
          
          this.dataSource = new MatTableDataSource<ChildReservation>(reservations);
          this.reservations = reservations;
        })
      })
    
    });

  }

  ngOnInit() {
    
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }
  
  call() {
    console.log(this.selection.selected);
    this.selection.selected.forEach(res => {
      console.log("---- Going to delete reservation " + res.id + " ----");      
      this.dataService.deleteChildReservation(res.id).subscribe();
    });
    
    console.log("--- Change route ---");    
    this.router.navigate(['/user-booking']);

  }

}

function delay(ms: number) {
  return new Promise( resolve => setTimeout(resolve, ms) );
}
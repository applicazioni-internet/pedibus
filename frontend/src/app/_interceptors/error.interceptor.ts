import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthenticationService } from '../authentication.service';


export class ErrorInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(e => {
            if ([401, 403].indexOf(e.status) !== -1) {
                // Auto logout if 401 or 403 response
                this.authenticationService.logout();
                // location.reload(true);
            }

            const error = e.error.message || e.statusText;
            return throwError(error);
        }));
    }
}

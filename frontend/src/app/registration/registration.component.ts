import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { AuthenticationService } from '../authentication.service';
import { PasswordMatchValidator } from '../_shared/password-match-validator.directive';
import { Router } from '@angular/router';
import { Child } from '../data/child';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  private registerForm: FormGroup;
  validationMessages: any;

  constructor(private fb: FormBuilder, private authService: AuthenticationService, private router: Router) {
    this.validationMessages = {
      email: [
        { type: 'required', message: 'Email is required' },
        { type: 'email', message: 'Email must be a valid address' },
        { type: 'minlength', message: 'Email too short' },
        { type: 'maxlength', message: 'Email too long' }
      ],
      firstName: [
        { type: 'required', message: 'First name is required' },
        { type: 'minlength', message: 'First name too short' },
        { type: 'maxlength', message: 'First name too long' }
      ],
      lastName: [
        { type: 'required', message: 'Last name is required' },
        { type: 'minlength', message: 'Last name too short' },
        { type: 'maxlength', message: 'Last name too long' }
      ],
      password: [
        { type: 'required', message: 'Password is required' },
        { type: 'pattern', message: 'Password must contain a number, a lowercase character and an uppercase character' },
        { type: 'minlength', message: 'Password too short' },
        { type: 'maxlength', message: 'Password too long' }
      ],
      birthDate: [
        {type: 'required', message: 'Date is required' },
        {type: 'pattern', message: 'Date must be a valid' },
      ],
      privacyTerms: [
        { type: 'required', message: 'You must accept privacy terms and conditions' }
      ]
    };
  }

  ngOnInit() {
    this.generateFormGroup();
  }

  private generateFormGroup() {
    this.registerForm = this.fb.group({
      email: [null, [
        Validators.required,
        Validators.email,
        Validators.minLength(6),
        Validators.maxLength(64)
      ]],
      firstName: [null, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(32),
      ]],
      lastName: [null, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(32)
      ]],
      password: [null, [
        Validators.required,
        Validators.pattern(/(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])/),
        Validators.minLength(6),
        Validators.maxLength(64)
      ]],
      passwordConfirm: [null, [
        Validators.required,
        Validators.pattern(/(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])/),
        Validators.minLength(6),
        Validators.maxLength(64),
        PasswordMatchValidator
      ]],
      childs: this.fb.array([
        this.fb.group({
          firstName: [null, [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(32),
          ]],
          lastName: [null, [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(32)
          ]],
          birthDate: [null,[
          Validators.required
           //DateValidator.ptDate
          ]]
        })
      ]),
      privacyTerms: [null, [
        Validators.requiredTrue
      ]]
    }, {
      validators: PasswordMatchValidator
    });
  }

  removeChildFormControl(i) {
    let childsArray = this.registerForm.controls.childs as FormArray;
    childsArray.removeAt(i);
  }

  addChildFormControl() {
    let childsArray = this.registerForm.controls.childs as FormArray;
    let arraylen = childsArray.length;

    let newChildGroup: FormGroup = this.fb.group({
      firstName: [null, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(32),
      ]],
      lastName: [null, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(32)
      ]],
      birthDate: ['',[
      Validators.required
      ]]
    });

    childsArray.insert(arraylen, newChildGroup);
  }

  /*
  * This method receives the form group value
  */
 submit(validateForm: any): void {
  console.log("Validated Form");  
  console.log(validateForm);

  // TODO Contact service
  this.authService.registration(validateForm.firstName, validateForm.lastName, validateForm.email, validateForm.password, validateForm.passwordConfirm, validateForm.childs)
     .subscribe(
       response => {
         console.log(response);
         this.router.navigate(['/login']);
       },
       err => {
         console.error(err.message);
       },
       () => {
         console.log('completed');
       }
     );
  }

}

//Custom DateValidator 
export class DateValidator {

  static ptDate(control: FormControl): { [key: string]: any } {
      let ptDatePattern =  /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g;

      if (!control.value.match(ptDatePattern))
          return { "ptDate": true };

      return null;
  }

  static usDate(control: FormControl): { [key: string]: any } {
      let usDatePattern = /^02\/(?:[01]\d|2\d)\/(?:19|20)(?:0[048]|[13579][26]|[2468][048])|(?:0[13578]|10|12)\/(?:[0-2]\d|3[01])\/(?:19|20)\d{2}|(?:0[469]|11)\/(?:[0-2]\d|30)\/(?:19|20)\d{2}|02\/(?:[0-1]\d|2[0-8])\/(?:19|20)\d{2}$/;

      if (!control.value.match(usDatePattern))
          return { "usDate": true };

      return null;
  }
}

import { Component, OnInit } from '@angular/core';
import {DataService} from "../data.service";
import {Message} from '../data/message';
import {Observable} from "rxjs";
import {formatDate} from "@angular/common";

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent {
  private newMessage$: Observable<Message[]>;
  private newMessage: Message[] = [];
  private filtersLoaded: Promise<boolean>;

  private readed: Message[] = [];
  private newSelected: Message[] = [];
  private readedSelected: Message[] = [];
  constructor(private dataService: DataService) {
    this.loadData();
  }

  loadData() {
    console.log('LOAD DATA');

    this.newMessage$ = this.dataService.getMessage();
    this.newMessage$.subscribe(res => {
      res.forEach(r => {
        if (r.read) {
          this.readed.push(r);
        } else {
          this.newMessage.push(r);
        }
      });
      this.readed.sort((one, two) => (one.date > two.date ? -1 : 1));
      this.newMessage.sort((one, two) => (one.date > two.date ? -1 : 1));

      this.filtersLoaded = Promise.resolve(true);
      console.log("MESSAGGI:");
      console.log(this.newMessage);
      console.log("lenght:");
      console.log(this.newMessage.length);

    });
  }
  convertDate(date: Date) {
    formatDate(date, 'yyyy-MM-dd', 'en');
  }
  checkNew(message: Message): boolean {
    if (this.newSelected.includes(message)) {
     delete this.newSelected[this.newSelected.indexOf(message)];
   } else {
     this.newSelected.push(message);
   }
    return true;
  }
  checkReaded(message: Message): boolean {
    if (this.readedSelected.includes(message)) {
      delete this.readedSelected[this.readedSelected.indexOf(message)];
    } else {
      this.readedSelected.push(message);
    }
    return true;
  }
  deleteNew() {
    this.newSelected.forEach(message => {
      this.newMessage = this.newMessage.filter(function(value, index, arr) {
        return value !== message;
      });
    });
    this.newSelected = [];
  }
  readNew() {
      this.newSelected.forEach( message => {
        this.newMessage = this.newMessage.filter(function(value, index, arr) {
          return value !== message;
        });
        this.readed.push(message);
        this.dataService.updateMessageRead(message.id, { read : true}).subscribe(() => console.log("Letto"));
      });
      this.newSelected = [];
  }
  deleteReaded() {
    this.readedSelected.forEach(message => {
      this.readed = this.readed.filter(function(value, index, arr) {
        return value !== message;
      });
    });
    this.readedSelected = [];
  }
}

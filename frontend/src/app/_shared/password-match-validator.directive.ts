import { Directive } from '@angular/core';
import { ValidatorFn, FormGroup, ValidationErrors } from '@angular/forms';

export const PasswordMatchValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  const password = control.get('password');
  const passwordConfirm = control.get('passwordConfirm');
  return password && passwordConfirm && password.value !== passwordConfirm.value ? { passwordMismatch: true } : null;
};

@Directive({
  selector: '[appPasswordMatchValidator]'
})
export class PasswordMatchValidatorDirective {

  constructor() { }

}

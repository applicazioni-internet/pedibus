--
-- Retrieve user and roles
--
select u.id, u.email, string_agg(r.name, ', ' order by r.id)
from users u
join user_role ur on u.id = ur.user_id
join roles r on r.id = ur.role_id
group by u.id, u.email
order by u.id

--
-- Retrieve user and children
--
select u.id, u.email, string_agg(c.first_name || ' ' || c.last_name, ', ' order by c.first_name)
from child c
join users u on c.parent_id = u.id
group by u.id, u.email
order by u.id

--
-- Retrieve companions for each line
--
select (select name from line where id = line_id) as line,
  string_agg((select email from users where id = user_id), ', ') as companions
from line_companion
group by line_id
order by line_id
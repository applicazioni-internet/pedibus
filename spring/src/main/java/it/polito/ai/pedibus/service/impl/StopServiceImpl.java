package it.polito.ai.pedibus.service.impl;

import it.polito.ai.pedibus.exceptions.StopNotFoundException;
import it.polito.ai.pedibus.model.entities.StopEntity;
import it.polito.ai.pedibus.model.repositories.StopRepository;
import it.polito.ai.pedibus.service.StopService;
import it.polito.ai.pedibus.service.dtos.StopDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StopServiceImpl implements StopService {

    private final StopRepository stopRepository;

    @Autowired
    public StopServiceImpl(StopRepository stopRepository) {
        this.stopRepository = stopRepository;
    }

    @Override
    public StopDTO getStopById(Long id) throws StopNotFoundException {
        StopEntity s = stopRepository.findById(id).orElseThrow(StopNotFoundException::new);
        return StopDTO.fromEntity(
                s.getLine().toDto(),
                s);
    }

    @Override
    public boolean existsById(Long id) {
        return stopRepository.existsById(id);
    }
}

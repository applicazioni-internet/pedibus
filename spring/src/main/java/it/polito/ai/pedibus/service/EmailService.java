package it.polito.ai.pedibus.service;

import org.springframework.mail.SimpleMailMessage;

/**
 * Utility service for managing email messages.
 */
public interface EmailService {

    /**
     * Sends a {@link SimpleMailMessage}
     * @param message   the message to send
     */
    void sendEmail(SimpleMailMessage message);
}

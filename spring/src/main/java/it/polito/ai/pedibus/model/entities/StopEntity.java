package it.polito.ai.pedibus.model.entities;

import it.polito.ai.pedibus.service.dtos.StopDTO;
import it.polito.ai.pedibus.utils.Direction;
import lombok.*;

import javax.persistence.*;
import java.time.LocalTime;

@Entity
@Table(name = "stop")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StopEntity implements Comparable<StopEntity> {

    @Id
    @Column(updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "stop_generator")
    @SequenceGenerator(name = "stop_generator",
            sequenceName = "stop_sequence",
            allocationSize = 1
    )
    private Long id;

    @Column(nullable = false)
    private int sequence;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private double latitude;

    @Column(nullable = false)
    private double longitude;

    @Column(nullable = false)
    private LocalTime time;

    @Column(nullable = false)
    @Enumerated
    private Direction direction;

    @ManyToOne
    @JoinColumn(
            name = "line_id",
            nullable = false
    )
    @ToString.Exclude
    private LineEntity line;

    @Override
    public int compareTo(StopEntity other) {
        return Integer.compare(this.sequence, other.sequence);
    }

    public static StopEntity fromDto(StopDTO stop) {
        return StopEntity.builder()
                .id(stop.getId())
                .latitude(stop.getLatitude())
                .longitude(stop.getLongitude())
                .name(stop.getName())
                .time(stop.getTime())
                .build();
    }
}

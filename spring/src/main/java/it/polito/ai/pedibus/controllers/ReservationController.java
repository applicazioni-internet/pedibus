package it.polito.ai.pedibus.controllers;

import it.polito.ai.pedibus.controllers.viewmodels.ReservationRequest;
import it.polito.ai.pedibus.controllers.viewmodels.ReservationVM;
import it.polito.ai.pedibus.controllers.viewmodels.ReservationsByStop;
import it.polito.ai.pedibus.exceptions.ChildNotFoundException;
import it.polito.ai.pedibus.exceptions.ReservationNotFoundException;
import it.polito.ai.pedibus.exceptions.StopNotFoundException;
import it.polito.ai.pedibus.exceptions.UserNotFoundException;
import it.polito.ai.pedibus.service.ChildService;
import it.polito.ai.pedibus.service.LineService;
import it.polito.ai.pedibus.service.ReservationService;
import it.polito.ai.pedibus.service.StopService;
import it.polito.ai.pedibus.service.dtos.ChildDTO;
import it.polito.ai.pedibus.service.dtos.ReservationDTO;
import it.polito.ai.pedibus.service.dtos.StopDTO;
import it.polito.ai.pedibus.service.dtos.UserDto;
import it.polito.ai.pedibus.utils.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import javax.validation.constraints.FutureOrPresent;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

// TODO Secure these methods
@RestController
@RequestMapping("api/reservations")
@Slf4j
public class ReservationController {

    private final ReservationService reservationService;
    private final LineService lineService;
    private final ChildService childService;
    private final StopService stopService;

    @Autowired
    public ReservationController(ReservationService reservationService,
                                 LineService lineService,
                                 ChildService childService,
                                 StopService stopService) {
        this.reservationService = reservationService;
        this.lineService = lineService;
        this.childService = childService;
        this.stopService = stopService;
    }

    @GetMapping("/{line}/{date}")
    public ReservationsByStop getReservationsByLineAndDate(
            @PathVariable Long line,
            @PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {
        if (!lineService.existsById(line)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Line not found");
        }

        final List<ReservationDTO> reservations = reservationService.getAllReservationsByLineAndDate(line, date);
        return ReservationsByStop.fromDto(reservations);
    }

    @GetMapping("/children/{childId}")
    public Resources<ReservationVM> getReservationsByChildId(
            @PathVariable Long childId) {

        if (!childService.existsById(childId)) {
            final String message = String.format("Child %d not found", childId);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, message);
        }

        final List<ReservationDTO> reservations = reservationService.getAllReservationsByChild(childId);
        log.info(String.format("Reservations for child %d: %s", childId, reservations.toString()));
        return new Resources<>(reservations.stream()
                .map(Utils::toVM)
                .collect(Collectors.toList()),
                linkTo(methodOn(ReservationController.class)
                        .getReservationsByChildId(childId))
                        .withSelfRel());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteReservationsByLineAndDateAndId(
            @PathVariable Long id,
            Authentication authentication) {

        log.info(String.format("Delete Received for ReservationId: %d", id));
        try {
            ReservationDTO reservation = reservationService.getById(id);
            if (!userMatch(authentication, reservation.getChild().getParent())) {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN);
            }
            reservationService.deleteById(id);
            return ResponseEntity.noContent().build();
        } catch (ReservationNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Reservation not found");
        } catch (UserNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "User not found");
        }
    }

    @PostMapping("/{line}/{date}")
    public ResponseEntity<ReservationVM> postReservation(
            @PathVariable Long line,
            @PathVariable @FutureOrPresent @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
            @RequestBody @Valid ReservationRequest reservationRequest,
            BindingResult br) {

        if (br.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        log.info(String.format("POST ReservationRequest for line %d on date %s", line, date.format(DateTimeFormatter.ISO_DATE)));
        log.info(String.format("Request body: %s", reservationRequest.toString()));

        preliminaryChecks(reservationRequest, date, line);

        try {
            ChildDTO child = childService.getChildById(reservationRequest.getChildId());
            log.info(String.format("Got ChildDto: %s", child.toString()));

            StopDTO departure = stopService.getStopById(reservationRequest.getDepartureId());
            departure.setLine(lineService.getLineById(line));
            log.info(String.format("Got departure stop: %s", departure.toString()));

            StopDTO arrival = stopService.getStopById(reservationRequest.getArrivalId());
            arrival.setLine(lineService.getLineById(line));
            log.info(String.format("Got arrival stop: %s", arrival.toString()));

            ReservationDTO resTmp = reservationRequest.toDto(child, date, departure, arrival, reservationRequest.getPresence());
            //ReservationDTO r = reservationService.insertReservation(reservationRequest.toDto(child, date, departure, arrival, reservationRequest.getPresence()), line);
            ReservationDTO r = reservationService.insertReservation(resTmp, line);
            log.info(String.format("Reservation: %s", r.toString()));

//            r.getArrival().setLine(lineService.getLineById(line));
//            r.getDeparture().setLine(lineService.getLineById(line));
            ReservationVM reservation = Utils.toVM(r);
            log.info(String.format("Reservation: %s mapped to VM", reservation.toString()));
            //Resource<ReservationVM> reservation = new Resource<>(Utils.toVM(r, line));
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(reservation);
        } catch (UserNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "User not found");
        } catch (ChildNotFoundException e) {
            final String message = String.format("Child %d not found", reservationRequest.getChildId());
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, message);
        } catch (StopNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    // TODO Body response?
    @PutMapping("/{id}")
    public ResponseEntity<ReservationVM> updateReservation(
            @PathVariable Long id) {

        try {
            ReservationDTO currentReservation = reservationService.getById(id);
            /*if (!userMatch(authentication, currentReservation.getChild().getParent())) {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN);
            }*/

            boolean presence = currentReservation.isPresent();
            presence = !presence;

            //currentReservation.setPresent(presence);
            //ReservationDTO newReservation = currentReservation;
            reservationService.updateReservation(currentReservation, presence);
            return ResponseEntity.ok().build();
        } catch (ReservationNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Reservation not found");
        } catch (UserNotFoundException | StopNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private boolean userMatch(Authentication authentication, UserDto user) {
        return authentication.getName().equals(user.getUsername());
    }

    private void preliminaryChecks(ReservationRequest reservationRequest, LocalDate date, Long line) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        log.debug(String.format("Received date: %s", date.toString()));
        String currentDate = sdf.format(new Date());
        log.debug(String.format("Received date: %s", currentDate));

        if (date.toString().compareTo(currentDate) < 0) {
            //if (sdf.format(date).compareTo(sdf.format(new Date())) < 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        if (!lineService.existsById(line)) {
            final String message = String.format("Line %d not found", line);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, message);
        }

        if (!stopService.existsById(reservationRequest.getDepartureId())) {
            final String message = String.format("Stop %d not found", reservationRequest.getDepartureId());
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, message);
        }

        if (!stopService.existsById(reservationRequest.getArrivalId())) {
            final String message = String.format("Stop %d not found", reservationRequest.getArrivalId());
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, message);
        }
    }
}

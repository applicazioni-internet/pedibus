package it.polito.ai.pedibus.service.impl;

import it.polito.ai.pedibus.exceptions.UserNotFoundException;
import it.polito.ai.pedibus.exceptions.VerificationTokenNotFoundException;
import it.polito.ai.pedibus.model.entities.UserEntity;
import it.polito.ai.pedibus.model.entities.VerificationTokenEntity;
import it.polito.ai.pedibus.model.repositories.UserRepository;
import it.polito.ai.pedibus.model.repositories.VerificationTokenRepository;
import it.polito.ai.pedibus.service.VerificationTokenService;
import it.polito.ai.pedibus.service.dtos.UserDto;
import it.polito.ai.pedibus.service.dtos.VerificationTokenDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.UUID;

import static it.polito.ai.pedibus.model.entities.VerificationTokenEntity.MILLISECONDS_VALIDITY;

/**
 * The implementation of {@link VerificationTokenService}
 */
@Service
@Transactional
public class VerificationTokenServiceImpl implements VerificationTokenService {

    private final UserRepository users;
    private final VerificationTokenRepository tokens;

    @Autowired
    public VerificationTokenServiceImpl(UserRepository users, VerificationTokenRepository tokens) {
        this.users = users;
        this.tokens = tokens;
    }

    @Override
    public String insertToken(UserDto userDto) throws UserNotFoundException {
        final Date expiringDate = new Date(System.currentTimeMillis() + MILLISECONDS_VALIDITY);
        final UUID uuid = UUID.randomUUID();
        final UserEntity userEntity = users.findByEmail(userDto.getUsername())
                .orElseThrow(UserNotFoundException::new);
        final VerificationTokenEntity token = VerificationTokenEntity.builder()
                .expiringDate(expiringDate)
                .token(uuid.toString())
                .user(userEntity)
                .build();
        tokens.save(token);

        return token.getToken();
    }

    @Override
    public VerificationTokenDto loadToken(String randomUUID) throws VerificationTokenNotFoundException {
        final VerificationTokenEntity token = tokens.findByToken(randomUUID)
                .orElseThrow(VerificationTokenNotFoundException::new);
        return  VerificationTokenDto.fromEntity(token);
    }

    @Override
    public void deleteToken(VerificationTokenDto token) {
        tokens.deleteByToken(token.getToken());
    }
}

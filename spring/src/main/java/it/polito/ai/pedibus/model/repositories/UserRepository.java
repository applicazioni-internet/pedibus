package it.polito.ai.pedibus.model.repositories;

import it.polito.ai.pedibus.model.entities.ChildEntity;
import it.polito.ai.pedibus.model.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    Optional<UserEntity> findById(Long id);

    Optional<UserEntity> findByEmail(String email);

    Optional<UserEntity> findByChildren(ChildEntity child);

    boolean existsByEmail(String email);
}

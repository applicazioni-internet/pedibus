package it.polito.ai.pedibus.utils;

public class Roles {
    public final static String ROLE_ADMINISTRATOR = "ROLE_ADMIN";
    public final static String ROLE_SUPER_ADMINISTRATOR = "ROLE_SUPER_ADMIN";
    public final static String ROLE_USER = "ROLE_USER";
}

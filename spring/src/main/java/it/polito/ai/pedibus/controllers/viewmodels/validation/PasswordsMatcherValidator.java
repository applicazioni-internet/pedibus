package it.polito.ai.pedibus.controllers.viewmodels.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordsMatcherValidator implements ConstraintValidator<PasswordsMatch, PasswordsMatcher> {

    @Override
    public void initialize(PasswordsMatch constraintAnnotation) {

    }

    @Override
    public boolean isValid(PasswordsMatcher passwordsMatcher, ConstraintValidatorContext constraintValidatorContext) {
        return passwordsMatcher.getPassword().equals(passwordsMatcher.getPasswordConfirm());
    }
}

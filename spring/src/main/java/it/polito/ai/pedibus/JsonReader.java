package it.polito.ai.pedibus;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import it.polito.ai.pedibus.model.entities.*;
import it.polito.ai.pedibus.model.json.LineJson;
import it.polito.ai.pedibus.model.json.StopJson;
import it.polito.ai.pedibus.model.json.UserJson;
import it.polito.ai.pedibus.model.repositories.*;
import it.polito.ai.pedibus.utils.Direction;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.ResourcePatternUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static it.polito.ai.pedibus.utils.Roles.*;

@Component
@ConditionalOnExpression("false") // TODO Abilitare se necessaria lettura da file
@Slf4j
public class JsonReader implements CommandLineRunner {

    private final ResourceLoader resourceLoader;
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final ChildRepository childRepository;
    private final LineRepository lineRepository;
    private final StopRepository stopRepository;
    private final RoleRepository roleRepository;

    @Autowired
    public JsonReader(ResourceLoader resourceLoader,
                      PasswordEncoder passwordEncoder,
                      UserRepository userRepository,
                      ChildRepository childRepository,
                      LineRepository lineRepository,
                      StopRepository stopRepository,
                      RoleRepository roleRepository) {
        this.resourceLoader = resourceLoader;
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.childRepository = childRepository;
        this.lineRepository = lineRepository;
        this.stopRepository = stopRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        File userFile = ResourceUtils.getFile("classpath:static/json/users.json");
        Resource[] resources = ResourcePatternUtils.getResourcePatternResolver(resourceLoader)
                .getResources("classpath:static/json/lines/*json");

        // Add roles to database
        RoleEntity roleSuperUser = RoleEntity.builder()
                .name(ROLE_SUPER_ADMINISTRATOR)
                .build();
        roleRepository.save(roleSuperUser);
        RoleEntity roleAdmin = RoleEntity.builder()
                .name(ROLE_ADMINISTRATOR)
                .build();
        RoleEntity roleUser = RoleEntity.builder()
                .name(ROLE_USER)
                .build();
        Set<RoleEntity> roles = new HashSet<>(Arrays.asList(roleUser, roleAdmin));
        roleRepository.saveAll(roles);

        // Add a super user
        final String superUserPassword = "SuperUser00";
        UserEntity superUser = UserEntity.builder()
                .email("superuser@mail.it")
                .firstName("Super")
                .lastName("User")
                .roles(new HashSet<>(Arrays.asList(roleUser, roleAdmin, roleSuperUser)))
                .password(passwordEncoder.encode(superUserPassword))
                .enabled(true)
                .build();
        userRepository.save(superUser);

        // Get all users
        List<UserJson> users = objectMapper.readValue(userFile, new TypeReference<List<UserJson>>() {});
        users.forEach(u -> {
                    UserEntity userEntity = UserEntity.builder()
                            .email(u.getEmail())
                            .firstName(u.getFirstName())
                            .lastName(u.getLastName())
                            .password(passwordEncoder.encode(u.getPassword()))
                            .enabled(true)
                            .roles(roles)
                            .build();
                    userRepository.save(userEntity);
                    u.getChildren().stream()
                            .map(c -> ChildEntity.builder()
                                    .firstName(c.getFirstName())
                                    .lastName(c.getLastName())
                                    .birthDate(c.getBirthDate())
                                    .parent(userEntity)
                                    .build())
                            .forEach(childRepository::save);
                });

        // Get all lines
        Map<String, LineJson> lines = new HashMap<>();
        for (Resource r : resources) {
            File f = r.getFile();
            LineJson l = objectMapper.readValue(f, LineJson.class);
            String name = f.getName().replace(".json", ""); // Clean file name
            lines.put(capitalizeFirst(name), l);
        }

        List<LineEntity> lineEntities = new ArrayList<>();
        List<StopEntity> stopEntities = new ArrayList<>();

        for (Map.Entry<String, LineJson> entry : lines.entrySet()) {
            String name = entry.getKey();
            LineJson line = entry.getValue();

            LineEntity l = LineEntity.builder()
                    .name(name)
                    .build();

            AtomicInteger counterForward = new AtomicInteger(0);
            AtomicInteger counterBackward = new AtomicInteger(line.getStops().size());

            for (StopJson sj : line.getStops()) {
                // Forward stop
                StopEntity sf = StopEntity.builder()
                        .name(sj.getName())
                        .time(sj.getTimeForward())
                        .latitude(sj.getLatitude())
                        .longitude(sj.getLongitude())
                        .sequence(counterForward.getAndIncrement())
                        .line(l)
                        .direction(Direction.FORWARD)
                        .build();

                // Backward stop
                StopEntity sb = StopEntity.builder()
                        .name(sj.getName())
                        .time(sj.getTimeBackward())
                        .latitude(sj.getLatitude())
                        .longitude(sj.getLongitude())
                        .sequence(counterBackward.getAndDecrement())
                        .line(l)
                        .direction(Direction.BACKWARD)
                        .build();

                stopEntities.add(sf);
                stopEntities.add(sb);
            }

            lineEntities.add(l);
        }

        // Sort to better understanding db
        stopEntities.sort(Comparator.comparing(StopEntity::getLine)
                .thenComparing(StopEntity::getSequence));

        lineRepository.saveAll(lineEntities);
        stopRepository.saveAll(stopEntities);
    }

    private String capitalizeFirst(String str) {
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }
}

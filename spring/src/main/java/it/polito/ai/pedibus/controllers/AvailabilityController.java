package it.polito.ai.pedibus.controllers;

import it.polito.ai.pedibus.controllers.viewmodels.Availability;
import it.polito.ai.pedibus.controllers.viewmodels.AvailabilityProposal;
import it.polito.ai.pedibus.exceptions.AvailabilityNotFoundException;
import it.polito.ai.pedibus.exceptions.LineNotFoundException;
import it.polito.ai.pedibus.service.AvailabilityService;
import it.polito.ai.pedibus.service.dtos.AvailabilityDto;
import it.polito.ai.pedibus.service.dtos.UserDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static it.polito.ai.pedibus.controllers.ControllerUtils.getErrorMessages;
import static it.polito.ai.pedibus.controllers.ControllerUtils.isPrivilegedUser;
import static it.polito.ai.pedibus.utils.Roles.ROLE_ADMINISTRATOR;
import static it.polito.ai.pedibus.utils.Roles.ROLE_SUPER_ADMINISTRATOR;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("api/availabilities")
@Slf4j
public class AvailabilityController {

    private final AvailabilityService availabilityService;

    @Autowired
    public AvailabilityController(AvailabilityService availabilityService) {
        this.availabilityService = availabilityService;
    }

    @GetMapping("/line/{line}/date/{date}")
    public ResponseEntity<List<Availability>> getAllAvailabilitiesByDateAndLine(
            @PathVariable Long line,
            @PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {
        return ResponseEntity.ok().body(availabilityService.loadAvailabilitiesByDateAndLine(date, line).stream()
                .map(Availability::fromDto)
                .collect(Collectors.toList()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Resource<Availability>> getAvailabilityById(@PathVariable long id) {
        UserDto user = (UserDto) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        try {
            AvailabilityDto availabilityDto = availabilityService.loadById(id);
            if (! isPrivilegedUser(user) && ! user.getUsername().equals(availabilityDto.getUser())) {
                String message = "You must be a privileged user to retrieve this availability";
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, message);
            }

            return ResponseEntity.ok().body(new Resource<>(Availability.fromDto(availabilityDto),
                    linkTo(methodOn(AvailabilityController.class)
                            .getAvailabilityById(id))
                            .withSelfRel()));
        } catch (AvailabilityNotFoundException e) {
            String message = "Availability " + id + " not found";
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, message);
        }
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<List<Availability>> getAvailabilitiesByUserId(
            @PathVariable long id) {

        UserDto user = (UserDto) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (! isPrivilegedUser(user) && user.getId() != id) {
            String message = "You must be a privileged user to retrieve this availability";
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, message);
        }

        List<AvailabilityDto> availabilities = availabilityService.loadByUserId(id);
        return ResponseEntity.ok().body(availabilities.stream()
                .map(Availability::fromDto)
                .collect(Collectors.toList()));
    }

    @GetMapping("/user/{user}/date/{date}")
    public ResponseEntity<List<Availability>> getAvailabilitiesByUserIdAndDate(
            @PathVariable Long user,
            @PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {

        UserDto userDto = (UserDto) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (! isPrivilegedUser(userDto) && ! userDto.getId().equals(user)) {
            String message = "You must be a privileged user to retrieve this information";
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, message);
        }

        List<AvailabilityDto> availabilities = availabilityService.loadAvailabilitiesByDateAndUser(date, user);
        return ResponseEntity.ok().body(availabilities.stream()
                        .map(Availability::fromDto)
                        .collect(Collectors.toList()));
    }

    @PostMapping("")
    public ResponseEntity<Resource<Availability>> insertAvailabilityProposal(
            @RequestBody @Valid AvailabilityProposal proposal,
            BindingResult br) {

        if (br.hasErrors()) {
            String message = "Data contains errors: " + getErrorMessages(br);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, message);
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        try {
            AvailabilityDto availabilityProposal = AvailabilityDto.fromAvailabilityProposal(proposal, authentication.getName());
            AvailabilityDto availabilityStored = availabilityService.insertAvailability(availabilityProposal);
            Availability vm = Availability.fromDto(availabilityStored);

            URI uri = MvcUriComponentsBuilder.fromController(AvailabilityController.class)
                    .path("/{id}")
                    .buildAndExpand(vm.getId())
                    .toUri();

            return ResponseEntity.created(uri)
                    .body(new Resource<>(vm,
                            linkTo(methodOn(AvailabilityController.class)
                                    .getAvailabilityById(vm.getId()))
                                    .withSelfRel()));
        } catch (LineNotFoundException e) {
            String message = "Line " + proposal.getLineId() + " not found";
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, message);
        } catch (UsernameNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("{id}")
    @Secured({ROLE_ADMINISTRATOR, ROLE_SUPER_ADMINISTRATOR})
    public ResponseEntity<Resource<Availability>> approveAvailability(@PathVariable long id) {
        try {
            AvailabilityDto updatedAvailability = availabilityService.approveAvailability(id);
            return ResponseEntity.ok(new Resource<>(Availability.fromDto(updatedAvailability),
                            linkTo(methodOn(AvailabilityController.class)
                                    .getAvailabilityById(id))
                                    .withSelfRel()));
        } catch (AvailabilityNotFoundException e) {
            String message = "Availability " + id + " not found";
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, message);
        }
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteAvailabilityById(@PathVariable long id) {
        try {
            UserDto user = (UserDto) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            AvailabilityDto availability = availabilityService.loadById(id);
            if (! isPrivilegedUser(user) && ! user.getUsername().equals(availability.getUser())) {
                String message = "You must be a privileged user to delete this availability";
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, message);
            }
            availabilityService.deleteAvailabilityById(id);
            return ResponseEntity.noContent().build();
        } catch (AvailabilityNotFoundException e) {
            String message = "Availability " + id + " not found";
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, message);
        }
    }
}

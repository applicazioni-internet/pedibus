package it.polito.ai.pedibus.service;

import it.polito.ai.pedibus.exceptions.UserNotFoundException;
import it.polito.ai.pedibus.exceptions.VerificationTokenNotFoundException;
import it.polito.ai.pedibus.service.dtos.UserDto;
import it.polito.ai.pedibus.service.dtos.VerificationTokenDto;

/**
 * Utility service for managing verification tokens.
 */
public interface VerificationTokenService {

    /**
     * Returns a {@link String} object that identifies the random UUID of the generated token.
     * <p>The user argument must specify the user for which the token is generated.</p>
     * @param user  the user for which the token is generated
     * @return      the generated random UUID
     * @throws UserNotFoundException If the user cannot be found
     */
    String insertToken(UserDto user) throws UserNotFoundException;

    /**
     * Returns a {@link VerificationTokenDto} object that identifies the token.
     * <p>The randomUUID argument must specify the UUID of the token to retrieve.</p>
     * @param randomUUID    the UUID of the token
     * @return              the retrieved token
     * @throws VerificationTokenNotFoundException If the token cannot be found
     */
    VerificationTokenDto loadToken(String randomUUID) throws VerificationTokenNotFoundException;

    /**
     * Deletes a {@link VerificationTokenDto} from the database.
     * @param token the token to delete
     */
    void deleteToken(VerificationTokenDto token);
}

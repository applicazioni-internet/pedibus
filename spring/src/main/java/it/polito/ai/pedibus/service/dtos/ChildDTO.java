package it.polito.ai.pedibus.service.dtos;

import it.polito.ai.pedibus.controllers.viewmodels.RegistrationRequest;
import it.polito.ai.pedibus.model.entities.ChildEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.LocalDate;
import java.util.List;

@Builder
@Data
public class ChildDTO {

	private Long id;
	private String firstName;
    private String lastName;
    private LocalDate birthDate;

    @ToString.Exclude @EqualsAndHashCode.Exclude
    private UserDto parent;

    @ToString.Exclude @EqualsAndHashCode.Exclude
    private List<ReservationDTO> reservations;

    public static ChildDTO fromRegistrationRequest(RegistrationRequest.Child c) {
        return ChildDTO.builder()
                .birthDate(c.getBirthDate())
                .firstName(c.getFirstName())
                .lastName(c.getLastName())
                .build();
    }

    public static ChildDTO fromEntity(ChildEntity entity, UserDto parent) {
        return ChildDTO.builder()
                .id(entity.getId())
                .firstName(entity.getFirstName())
                .lastName(entity.getLastName())
                .birthDate(entity.getBirthDate())
                .parent(parent)
                .build();
    }
}

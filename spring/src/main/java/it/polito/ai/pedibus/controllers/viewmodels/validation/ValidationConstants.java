package it.polito.ai.pedibus.controllers.viewmodels.validation;

public class ValidationConstants {

    public final static int MIN_PASSWORD_LENGTH	= 6;
    public final static int MAX_PASSWORD_LENGTH	= 64;
    public final static String PASSWORD_REGEX	= "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{" +
            MIN_PASSWORD_LENGTH + "," +
            MAX_PASSWORD_LENGTH + "})";
    public final static int MIN_NAME_LENGTH		= 2;
    public final static int MAX_NAME_LENGTH		= 32;
    public final static int MAX_EMAIL_LENGTH	= 64;
    public final static int MAX_STRING_LENGTH	= 64;
}

package it.polito.ai.pedibus.service.dtos;

import it.polito.ai.pedibus.model.entities.MessageEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
public class MessageDto {

    private Long id;
    private String sender;
    private String recipient;
    private String body;
    private LocalDateTime date;
    private boolean read;

    public static MessageDto fromEntity(MessageEntity entity) {
        return MessageDto.builder()
                .id(entity.getId())
                .body(entity.getBody())
                .date(entity.getDate())
                .sender(entity.getSender().getEmail())
                .recipient(entity.getRecipient().getEmail())
                .read(entity.getRead())
                .build();
    }
}

package it.polito.ai.pedibus.service.impl;

import it.polito.ai.pedibus.exceptions.PasswordResetTokenNotFoundException;
import it.polito.ai.pedibus.exceptions.UserNotEnabledException;
import it.polito.ai.pedibus.exceptions.UserNotFoundException;
import it.polito.ai.pedibus.model.entities.PasswordResetTokenEntity;
import it.polito.ai.pedibus.model.entities.UserEntity;
import it.polito.ai.pedibus.model.repositories.PasswordResetTokenRepository;
import it.polito.ai.pedibus.model.repositories.UserRepository;
import it.polito.ai.pedibus.service.PasswordResetTokenService;
import it.polito.ai.pedibus.service.dtos.PasswordResetTokenDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.UUID;

import static it.polito.ai.pedibus.model.entities.PasswordResetTokenEntity.MILLISECONDS_VALIDITY;

/**
 * The implementation of {@link PasswordResetTokenService}
 */
@Service
@Transactional
public class PasswordResetTokenServiceImpl implements PasswordResetTokenService {

    private final UserRepository users;
    private final PasswordResetTokenRepository tokens;

    @Autowired
    public PasswordResetTokenServiceImpl(UserRepository users, PasswordResetTokenRepository tokens) {
        this.users = users;
        this.tokens = tokens;
    }

    @Override
    public String insertToken(String email) throws UserNotFoundException, UserNotEnabledException {
        final Date expiringDate = new Date(System.currentTimeMillis() + MILLISECONDS_VALIDITY);
        final UUID uuid = UUID.randomUUID();
        final UserEntity userEntity = users.findByEmail(email)
                .orElseThrow(UserNotFoundException::new);
        if (!userEntity.isEnabled()) {
            throw new UserNotEnabledException();
        }
        final PasswordResetTokenEntity token = PasswordResetTokenEntity.builder()
                .expiringDate(expiringDate)
                .token(uuid.toString())
                .user(userEntity)
                .build();
        tokens.save(token);

        return token.getToken();
    }

    @Override
    public PasswordResetTokenDto loadToken(String randomUUID) throws PasswordResetTokenNotFoundException {
        final PasswordResetTokenEntity token = tokens.findByToken(randomUUID)
                .orElseThrow(PasswordResetTokenNotFoundException::new);
        return PasswordResetTokenDto.fromEntity(token);
    }

    @Override
    public void deleteToken(PasswordResetTokenDto token) {
        tokens.deleteByToken(token.getToken());
    }
}

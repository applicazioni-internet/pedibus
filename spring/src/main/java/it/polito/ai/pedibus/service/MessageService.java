package it.polito.ai.pedibus.service;

import it.polito.ai.pedibus.exceptions.MessageNotFoundException;
import it.polito.ai.pedibus.exceptions.UserNotFoundException;
import it.polito.ai.pedibus.service.dtos.MessageDto;

import java.util.List;

public interface MessageService {

    MessageDto getMessageById(long id) throws MessageNotFoundException;

    MessageDto sendMessage(MessageDto message) throws UserNotFoundException;

    List<MessageDto> getMessagesBySenderId(long id);

    List<MessageDto> getMessagesByRecipientId(long id);

    MessageDto updateMessage(long id, boolean read) throws MessageNotFoundException;
}

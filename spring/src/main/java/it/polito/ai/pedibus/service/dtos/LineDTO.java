package it.polito.ai.pedibus.service.dtos;

import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Set;

@Builder
@Data
public class LineDTO {
	private Long id;
	private String name;
	private List<StopDTO> forwards;
	private List<StopDTO> backwards;
	private Set<UserDto> companions;
}

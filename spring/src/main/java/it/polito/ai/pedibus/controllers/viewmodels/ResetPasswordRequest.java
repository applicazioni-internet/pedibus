package it.polito.ai.pedibus.controllers.viewmodels;

import it.polito.ai.pedibus.controllers.viewmodels.validation.PasswordsMatch;
import it.polito.ai.pedibus.controllers.viewmodels.validation.PasswordsMatcher;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static it.polito.ai.pedibus.controllers.viewmodels.validation.ValidationConstants.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@PasswordsMatch
public class ResetPasswordRequest implements PasswordsMatcher {

	@NotNull
	@Size(min = MIN_PASSWORD_LENGTH, max = MAX_PASSWORD_LENGTH)
	@Pattern(regexp = PASSWORD_REGEX)
	private String password;

	@NotNull
	@Size(min = MIN_PASSWORD_LENGTH, max = MAX_PASSWORD_LENGTH)
	private String passwordConfirm;
}

package it.polito.ai.pedibus.controllers.viewmodels;

import it.polito.ai.pedibus.service.dtos.ReservationDTO;
import it.polito.ai.pedibus.utils.Direction;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Builder
@Data
public class ReservationsByStop {

    private List<Stop> forward;
    private List<Stop> backward;

    @Builder
    @Data
    public static class Stop {
        private Long id;
        private List<InnerReservation> present;
        private List<InnerReservation> absent;
    }

    @AllArgsConstructor
    @Data
    public static class InnerReservation {
        private Long childId;
        private String childName;
        private Long reservation;
    }

    public static ReservationsByStop fromDto(Collection<ReservationDTO> dtos) {
        Map<Long, List<ReservationDTO>> f = dtos.stream()
                .filter(r -> r.getDeparture().getDirection().equals(Direction.FORWARD))
                .collect(Collectors.groupingBy(r -> r.getDeparture().getId()));
        Map<Long, List<ReservationDTO>> b = dtos.stream()
                .filter(r -> r.getDeparture().getDirection().equals(Direction.BACKWARD))
                .collect(Collectors.groupingBy(r -> r.getDeparture().getId()));
        List<Stop> forward = f.entrySet().stream()
                .map(e -> Stop.builder()
                        .id(e.getKey())
                        .absent(e.getValue().stream()
                                .filter(r -> !r.isPresent())
                                .map(r -> new InnerReservation(
                                        r.getChild().getId(),
                                        r.getChild().getFirstName() + " " + r.getChild().getLastName(),
                                        r.getId()))
                                .collect(Collectors.toList()))
                        .present((e.getValue().stream()
                                .filter(ReservationDTO::isPresent)
                                .map(r -> new InnerReservation(
                                        r.getChild().getId(),
                                        r.getChild().getFirstName() + " " + r.getChild().getLastName(),
                                        r.getId()))
                                .collect(Collectors.toList())))
                        .build())
                .collect(Collectors.toList());
        List<Stop> backward = b.entrySet().stream()
                .map(e -> Stop.builder()
                        .id(e.getKey())
                        .absent(e.getValue().stream()
                                .filter(r -> !r.isPresent())
                                .map(r -> new InnerReservation(
                                        r.getChild().getId(),
                                        r.getChild().getFirstName() + " " + r.getChild().getLastName(),
                                        r.getId()))
                                .collect(Collectors.toList()))
                        .present((e.getValue().stream()
                                .filter(ReservationDTO::isPresent)
                                .map(r -> new InnerReservation(
                                        r.getChild().getId(),
                                        r.getChild().getFirstName() + " " + r.getChild().getLastName(),
                                        r.getId()))
                                .collect(Collectors.toList())))
                        .build())
                .collect(Collectors.toList());

        return ReservationsByStop.builder()
                .backward(backward)
                .forward(forward)
                .build();
    }
}

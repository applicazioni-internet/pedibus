package it.polito.ai.pedibus.model.entities;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "reservation")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReservationEntity {

    @Id
    @Column(updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "reservation_generator")
    @SequenceGenerator(
            name = "reservation_generator",
            sequenceName = "reservation_sequence",
            allocationSize = 1
    )
    private Long id;

    @Column(nullable = false)
    private LocalDate date;

    @Column(columnDefinition = "boolean default false")
    private boolean presence;

    @ManyToOne
    @JoinColumn(
            name = "child_id",
            nullable = false
    )
    private ChildEntity child;

    @ManyToOne
    @JoinColumn(
            name = "departure_stop_id",
            nullable = false
    )
    private StopEntity departure;

    @ManyToOne
    @JoinColumn(
            name = "arrival_stop_id",
            nullable = false
    )
    private StopEntity arrival;
}

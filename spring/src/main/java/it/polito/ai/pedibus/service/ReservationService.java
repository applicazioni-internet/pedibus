package it.polito.ai.pedibus.service;

import it.polito.ai.pedibus.service.dtos.ReservationDTO;
import it.polito.ai.pedibus.exceptions.ReservationNotFoundException;
import it.polito.ai.pedibus.exceptions.StopNotFoundException;

import java.time.LocalDate;
import java.util.List;

public interface ReservationService {
	List<ReservationDTO> getAllReservationsByLineAndDate(Long lineId, LocalDate date);

	List<ReservationDTO> getAllReservationsByChild(Long childId);

	ReservationDTO getById(Long id) throws ReservationNotFoundException;

	ReservationDTO insertReservation(ReservationDTO reservation, Long lineId);

	void deleteById(Long id) throws ReservationNotFoundException;

	ReservationDTO updateReservation(ReservationDTO reservation, boolean presence) throws ReservationNotFoundException, StopNotFoundException;
}

package it.polito.ai.pedibus.service.dtos;

import it.polito.ai.pedibus.controllers.viewmodels.RegistrationRequest;
import it.polito.ai.pedibus.model.entities.LineEntity;
import it.polito.ai.pedibus.model.entities.UserEntity;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;
import java.util.stream.Collectors;

import static it.polito.ai.pedibus.utils.Roles.ROLE_USER;

/**
 * User DTO
 */
@Data
@Builder
@AllArgsConstructor
public class UserDto implements UserDetails {

    private Long id;
    private String username;
    @ToString.Exclude @EqualsAndHashCode.Exclude
    private String password;
    private String firstName;
    private String lastName;
    private boolean enabled;
    private Set<ChildDTO> children;
    private Set<GrantedAuthority> authorities;
    private Long[] companionLines;

    // TODO Add attributes

    /**
     * Returns a {@link UserDto} from a {@link UserEntity}
     * @param entity    the entity to convert
     * @return          the created dto.
     */
    public static UserDto fromEntity(UserEntity entity) {
        UserDto dto = UserDto.builder()
                .id(entity.getId())
                .username(entity.getEmail())
                .password(entity.getPassword())
                .firstName(entity.getFirstName())
                .lastName(entity.getLastName())
                .enabled(entity.isEnabled())
                .authorities(entity.getRoles().stream()
                        .map(r -> new GrantedAuthorityImpl(r.getName()))
                        .collect(Collectors.toSet()))
                .companionLines(entity.getCompanionLines().stream()
                        .map(LineEntity::getId)
                        .sorted()
                        .toArray(Long[]::new))
                .build();

        final Set<ChildDTO> children = entity.getChildren().stream()
                .map(c -> ChildDTO.fromEntity(c, dto))
                .collect(Collectors.toSet());

        dto.setChildren(children);
        return dto;
    }

    /**
     * Returns a {@link UserDto} from a {@link RegistrationRequest}
     * @param registrationRequest   the entity to convert
     * @return                      the created dto.
     */
    public static UserDto fromRegistrationRequest(RegistrationRequest registrationRequest) {
        GrantedAuthority ga = new GrantedAuthorityImpl(ROLE_USER);

        final UserDto user = UserDto.builder()
                .username(registrationRequest.getEmail())
                .firstName(registrationRequest.getFirstName())
                .lastName(registrationRequest.getLastName())
                .password(registrationRequest.getPassword())
                .enabled(false)
                .children(Arrays.stream(registrationRequest.getChildren())
                        .map(ChildDTO::fromRegistrationRequest)
                        .collect(Collectors.toSet()))
                .authorities(new HashSet<>(Arrays.asList(ga)))
                .build();

        user.getChildren().forEach(c -> c.setParent(user));

        return user;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
}

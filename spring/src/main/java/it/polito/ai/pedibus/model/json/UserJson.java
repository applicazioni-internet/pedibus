package it.polito.ai.pedibus.model.json;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import it.polito.ai.pedibus.utils.LocalDateDeserializer;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class UserJson {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private List<ChildJson> children;

    @Data
    public static class ChildJson {
        private String firstName;
        private String lastName;

        @JsonDeserialize(using = LocalDateDeserializer.class)
        private LocalDate birthDate;
    }
}

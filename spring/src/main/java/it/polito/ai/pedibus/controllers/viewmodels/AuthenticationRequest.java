package it.polito.ai.pedibus.controllers.viewmodels;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

import static it.polito.ai.pedibus.controllers.viewmodels.validation.ValidationConstants.*;

@Data
public class AuthenticationRequest implements Serializable {

    @NotNull
    @Email
    @Size(max = MAX_EMAIL_LENGTH)
    private String email;

    @NotNull
    @Size(min = MIN_PASSWORD_LENGTH, max = MAX_PASSWORD_LENGTH)
    private String password;
}

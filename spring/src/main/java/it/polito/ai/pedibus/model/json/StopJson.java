package it.polito.ai.pedibus.model.json;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import it.polito.ai.pedibus.utils.LocalTimeDeserializer;
import lombok.Data;

import java.time.LocalTime;

@Data
public class StopJson {
    private String name;
    private float latitude;
    private float longitude;

    @JsonDeserialize(using = LocalTimeDeserializer.class)
    private LocalTime timeForward;

    @JsonDeserialize(using = LocalTimeDeserializer.class)
    private LocalTime timeBackward;
}

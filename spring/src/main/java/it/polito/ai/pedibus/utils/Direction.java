package it.polito.ai.pedibus.utils;

public enum Direction {
    FORWARD,
    BACKWARD
}

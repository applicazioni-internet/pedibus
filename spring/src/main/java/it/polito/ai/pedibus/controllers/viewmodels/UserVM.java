package it.polito.ai.pedibus.controllers.viewmodels;

import it.polito.ai.pedibus.service.dtos.UserDto;
import it.polito.ai.pedibus.utils.Utils;
import lombok.Builder;
import lombok.Value;
import org.apache.catalina.User;

import java.util.List;

@Value
@Builder
public class UserVM implements Comparable<UserVM> {

	Long id;
	String firstName;
    String lastName;
    Boolean enabled;
    Long[] companionLines;
    List<ChildVM> children;
	String email;
	String role;

	@Override
	public int compareTo(UserVM o) {
		return Long.compare(this.id, o.id);
	}

	public static UserVM fromDto(UserDto dto) {
	    return Utils.toVM(dto);
    }
}

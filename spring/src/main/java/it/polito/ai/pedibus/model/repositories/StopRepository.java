package it.polito.ai.pedibus.model.repositories;

import it.polito.ai.pedibus.model.entities.StopEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StopRepository extends JpaRepository<StopEntity, Long> {
}

package it.polito.ai.pedibus.service.impl;

import it.polito.ai.pedibus.exceptions.ChildNotFoundException;
import it.polito.ai.pedibus.model.entities.ChildEntity;
import it.polito.ai.pedibus.model.repositories.ChildRepository;
import it.polito.ai.pedibus.model.repositories.ReservationRepository;
import it.polito.ai.pedibus.service.ChildService;
import it.polito.ai.pedibus.service.dtos.ChildDTO;
import it.polito.ai.pedibus.service.dtos.ReservationDTO;
import it.polito.ai.pedibus.service.dtos.UserDto;
import it.polito.ai.pedibus.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ChildServiceImpl implements ChildService {

    private final ChildRepository childRepository;
    private final ReservationRepository reservations;

    @Autowired
    public ChildServiceImpl(ChildRepository childRepository, ReservationRepository reservations) {
        this.childRepository = childRepository;
        this.reservations = reservations;
    }

    @Override
    public ChildDTO getChildById(Long id) throws ChildNotFoundException {
        final ChildEntity child = childRepository.findById(id).orElseThrow(ChildNotFoundException::new);
        final List<ReservationDTO> r = reservations.getAllByChildId(id).stream()
                .map(ReservationDTO::fromEntity)
                .collect(Collectors.toList());
        final UserDto u = UserDto.fromEntity(child.getParent());
        ChildDTO c = Utils.toDTO(child, r, u);
        c.getReservations()
                .forEach(t -> t.setChild(c));
        return c;
    }

    @Override
    public boolean existsById(Long id) {
        return childRepository.existsById(id);
    }
}

package it.polito.ai.pedibus.service.dtos;

import it.polito.ai.pedibus.model.entities.PasswordResetTokenEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

/**
 * Password reset token DTO
 */
@Data
@Builder
@AllArgsConstructor
public class PasswordResetTokenDto {

    private String token;
    private Date expiringDate;
    private UserDto user;

    /**
     * Returns a {@link PasswordResetTokenDto} from a {@link PasswordResetTokenEntity}
     * @param entity    the entity to convert
     * @return          the created dto.
     */
    public static PasswordResetTokenDto fromEntity(PasswordResetTokenEntity entity) {
        final UserDto user = UserDto.fromEntity(entity.getUser());
        return PasswordResetTokenDto.builder()
                .expiringDate(entity.getExpiringDate())
                .token(entity.getToken())
                .user(user)
                .build();
    }
}

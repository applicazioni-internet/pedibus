package it.polito.ai.pedibus.service.impl;

import it.polito.ai.pedibus.exceptions.ReservationNotFoundException;
import it.polito.ai.pedibus.exceptions.StopNotFoundException;
import it.polito.ai.pedibus.model.entities.ReservationEntity;
import it.polito.ai.pedibus.model.repositories.ReservationRepository;
import it.polito.ai.pedibus.service.LineService;
import it.polito.ai.pedibus.service.ReservationService;
import it.polito.ai.pedibus.service.UserService;
import it.polito.ai.pedibus.service.dtos.ChildDTO;
import it.polito.ai.pedibus.service.dtos.LineDTO;
import it.polito.ai.pedibus.service.dtos.ReservationDTO;
import it.polito.ai.pedibus.service.dtos.UserDto;
import it.polito.ai.pedibus.utils.Utils;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
class ReservationServiceImpl implements ReservationService {

    private final ReservationRepository reservationRepository;
    private final UserService userService;
    private final LineService lineService;

   @Autowired
    public ReservationServiceImpl(ReservationRepository reservationRepository,
                                  UserService userService,
                                  LineService lineService) {
        this.reservationRepository = reservationRepository;
        this.userService = userService;
        this.lineService = lineService;
    }

    @Override
    public List<ReservationDTO> getAllReservationsByLineAndDate(Long lineId, LocalDate date) {
        return reservationRepository.getAllByDeparture_Line_IdAndDate(lineId, date).stream()
                .map(ReservationDTO::fromEntity)
                .collect(Collectors.toList());
    }

    @Override
    public List<ReservationDTO> getAllReservationsByChild(Long childId) {
        return reservationRepository.getAllByChildIdAndDateAfter(childId, LocalDate.now()).stream()
                .map(ReservationDTO::fromEntity)
                .collect(Collectors.toList());
    }

    @Override
    public ReservationDTO getById(Long id) throws ReservationNotFoundException {
       final ReservationEntity r = reservationRepository.findById(id).orElseThrow(ReservationNotFoundException::new);
        return ReservationDTO.fromEntity(r);
    }

    @Override
    public ReservationDTO insertReservation(ReservationDTO reservation, Long lineId) {
        ReservationEntity r = Utils.toEntity(reservation);
        System.out.println("=== Insert Reservation Entity " + r.toString() + " ===");
        ReservationEntity newR = reservationRepository.save(r);
        LineDTO line = lineService.getLineById(lineId);

        UserDto parent = this.userService.getUserByChildId(r.getChild().getId());

        List<ReservationDTO> reservations = this.getAllReservationsByChild(reservation.getId());
        return Utils.toDTOInsert(newR, reservations, parent, line);
        //return Utils.toDTO(r);
    }

    @Override
    public void deleteById(Long id) throws ReservationNotFoundException {
        reservationRepository.deleteById(id);
    }

    @Override
    public ReservationDTO updateReservation(ReservationDTO reservation, boolean presence) throws ReservationNotFoundException, StopNotFoundException {
        ReservationEntity reservationEntity = reservationRepository.findById(reservation.getId()).orElseThrow(ReservationNotFoundException::new);
        reservationEntity.setPresence(presence);
        ReservationEntity newReservationEntity = reservationRepository.save(reservationEntity);
        return ReservationDTO.fromEntity(
                newReservationEntity,
                ChildDTO.fromEntity(
                        reservationEntity.getChild(),
                        UserDto.fromEntity(reservationEntity.getChild().getParent())));
    }

}

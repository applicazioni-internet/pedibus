package it.polito.ai.pedibus.controllers.viewmodels;

import it.polito.ai.pedibus.service.dtos.MessageDto;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Builder
@Data
public class Message implements Comparable<Message> {

    private long id;

    @NotNull
    private String body;

    @NotNull
    private String recipient;

    private String sender;

    private LocalDateTime date;

    private boolean read;

    @Override
    public int compareTo(Message o) {
        return Long.compare(this.id, o.id);
    }

    public static Message fromDto(MessageDto dto) {
        return Message.builder()
                .id(dto.getId())
                .body(dto.getBody())
                .date(dto.getDate())
                .recipient(dto.getRecipient())
                .sender(dto.getSender())
                .read(dto.isRead())
                .build();
    }
}

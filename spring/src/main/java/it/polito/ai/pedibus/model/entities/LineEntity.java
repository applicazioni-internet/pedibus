package it.polito.ai.pedibus.model.entities;

import it.polito.ai.pedibus.service.dtos.LineDTO;
import it.polito.ai.pedibus.utils.Utils;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "line")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {
        "companions",
        "stops"
})
public class LineEntity implements Comparable<LineEntity> {

    @Id
    @Column(updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "line_generator")
    @SequenceGenerator(
            name = "line_generator",
            sequenceName = "line_sequence",
            allocationSize = 1)
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;

    @ManyToMany
    @JoinTable(
            name = "line_companion",
            joinColumns = {@JoinColumn(name = "line_id", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "user_id", nullable = false)})
    private Set<UserEntity> companions;

    @OneToMany(mappedBy = "line")
    private Set<StopEntity> stops;

    @Override
    public int compareTo(LineEntity other) {
        return this.name.compareTo(other.name);
    }

    public static LineDTO toDto(LineEntity entity) {
        return Utils.toDTO(entity);
    }

    public LineDTO toDto() {
        return LineEntity.toDto(this);
    }
}

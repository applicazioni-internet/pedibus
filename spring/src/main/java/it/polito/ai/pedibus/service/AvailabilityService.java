package it.polito.ai.pedibus.service;

import it.polito.ai.pedibus.service.dtos.AvailabilityDto;

import java.time.LocalDate;
import java.util.List;

// TODO Document this class
public interface AvailabilityService {

    AvailabilityDto loadById(Long id);

    List<AvailabilityDto> loadAvailabilitiesByDateAndLine(LocalDate date, Long lineId);

    List<AvailabilityDto> loadAvailabilitiesByDateAndUser(LocalDate date, Long userId);

    AvailabilityDto insertAvailability(AvailabilityDto availabilityProposal);

    void deleteAvailabilityById(long id);

    List<AvailabilityDto> loadByUserId(long id);

    AvailabilityDto approveAvailability(long id);
}

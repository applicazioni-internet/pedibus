package it.polito.ai.pedibus.model.entities;

import it.polito.ai.pedibus.service.dtos.UserDto;
import lombok.*;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "users")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {
        "companionLines",
        "children",
        "roles"
})
public class UserEntity {

    @Id
	@Column(updatable = false, nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_generator")
	@SequenceGenerator(
			name = "user_generator",
			sequenceName = "user_sequence",
			allocationSize = 1
	)
	private Long id;

    @Column(nullable = false)
    @ToString.Exclude
    private String password;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false)
    private boolean enabled;

    @ManyToMany(mappedBy = "companions")
    @ToString.Exclude
    private Set<LineEntity> companionLines;

    @OneToMany(mappedBy = "parent")
    private Set<ChildEntity> children;

    @ManyToMany()
    @JoinTable(
            name = "user_role",
            joinColumns = {@JoinColumn(name = "user_id", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "role_id", nullable = false)})
    private Set<RoleEntity> roles;

    public static UserEntity fromDto(UserDto dto, RoleEntity role, PasswordEncoder passwordEncoder) {
        final UserEntity entity = UserEntity.builder()
                .email(dto.getUsername())
                .password(passwordEncoder.encode(dto.getPassword()))
                .firstName(dto.getFirstName())
                .lastName(dto.getLastName())
                .enabled(false)
                .roles(new HashSet<>(Arrays.asList(role)))
                .children(dto.getChildren().stream()
                        .map(ChildEntity::fromDto)
                        .collect(Collectors.toSet()))
                // TODO Lines
                .build();

        entity.getChildren().forEach(c -> c.setParent(entity));

        return entity;
    }
}

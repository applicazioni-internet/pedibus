package it.polito.ai.pedibus.model.repositories;

import it.polito.ai.pedibus.model.entities.LineEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LineRepository extends JpaRepository<LineEntity, Long> {
}

package it.polito.ai.pedibus.controllers.viewmodels;

import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Set;

@Builder
@Data
public class LineVM implements Comparable<LineVM> {

    private Long id;
	private String name;
    private Set<String> companions;
    private List<StopVM> forward;
    private List<StopVM> backward;

    @Override
    public int compareTo(LineVM o) {
        return Long.compare(this.id, o.id);
    }
}

package it.polito.ai.pedibus.model.repositories;

import it.polito.ai.pedibus.model.entities.ReservationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<ReservationEntity, Long> {
    List<ReservationEntity> getAllByDeparture_Line_IdAndDate(Long lineId, LocalDate date);

    List<ReservationEntity> getAllByChildIdAndDateAfter(Long childId, LocalDate date);

    List<ReservationEntity> getAllByChildId(Long childId);
}

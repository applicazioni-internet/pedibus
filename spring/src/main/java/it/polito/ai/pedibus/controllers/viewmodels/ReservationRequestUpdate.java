package it.polito.ai.pedibus.controllers.viewmodels;

import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.NotNull;

@Value
@Builder
public class ReservationRequestUpdate {
    @NotNull Boolean presence;
}

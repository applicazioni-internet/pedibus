package it.polito.ai.pedibus.controllers.viewmodels;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;

@Data
public class AvailabilityProposal implements Serializable {

    @NotNull
    private Long lineId;

    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Future
    private LocalDate date;
}

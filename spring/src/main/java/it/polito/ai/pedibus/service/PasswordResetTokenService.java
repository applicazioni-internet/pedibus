package it.polito.ai.pedibus.service;

import it.polito.ai.pedibus.exceptions.PasswordResetTokenNotFoundException;
import it.polito.ai.pedibus.exceptions.UserNotEnabledException;
import it.polito.ai.pedibus.exceptions.UserNotFoundException;
import it.polito.ai.pedibus.service.dtos.PasswordResetTokenDto;

/**
 * Utility service for managing password reset tokens.
 */
public interface PasswordResetTokenService {

    /**
     * Returns a {@link String} object that identifies the random UUID of the generated token.
     * <p>The email argument must specify the user for which the token is generated.</p>
     * @param email     the email of the user for which the token is generated
     * @return          the generated random UUID
     * @throws UserNotFoundException If the user cannot be found
     * @throws UserNotEnabledException If the user is found but not enabled
     */
    String insertToken(String email) throws UserNotFoundException, UserNotEnabledException;

    /**
     * Returns a {@link PasswordResetTokenDto} object that identifies the token.
     * <p>The randomUUID argument must specify the UUID of the token to retrieve.</p>
     * @param randomUUID    the UUID of the token
     * @return              the retrieved token
     * @throws PasswordResetTokenNotFoundException If the token cannot be found
     */
    PasswordResetTokenDto loadToken(String randomUUID) throws PasswordResetTokenNotFoundException;

    /**
     * Deletes a {@link PasswordResetTokenDto} from the database.
     * @param token the token to delete
     */
    void deleteToken(PasswordResetTokenDto token);
}

package it.polito.ai.pedibus.controllers;

import it.polito.ai.pedibus.controllers.viewmodels.*;
import it.polito.ai.pedibus.exceptions.*;
import it.polito.ai.pedibus.security.JwtTokenUtil;
import it.polito.ai.pedibus.service.EmailService;
import it.polito.ai.pedibus.service.PasswordResetTokenService;
import it.polito.ai.pedibus.service.UserService;
import it.polito.ai.pedibus.service.VerificationTokenService;
import it.polito.ai.pedibus.service.dtos.PasswordResetTokenDto;
import it.polito.ai.pedibus.service.dtos.UserDto;
import it.polito.ai.pedibus.service.dtos.VerificationTokenDto;
import it.polito.ai.pedibus.utils.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.authentication.*;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.HashSet;

@RestController
@RequestMapping("api/auth")
@Slf4j
public class AuthenticationController {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;
    private final UserService userService;
    private final VerificationTokenService verificationTokenService;
    private final PasswordResetTokenService passwordResetTokenService;
    private final EmailService emailService;

    @Autowired
    public AuthenticationController(AuthenticationManager authenticationManager,
                                    JwtTokenUtil jwtTokenUtil,
                                    UserService userService,
                                    VerificationTokenService verificationTokenService,
                                    PasswordResetTokenService passwordResetTokenService,
                                    EmailService emailService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userService = userService;
        this.verificationTokenService = verificationTokenService;
        this.passwordResetTokenService = passwordResetTokenService;
        this.emailService = emailService;
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody @Valid AuthenticationRequest authenticationRequest,
                                   BindingResult br) {
        if (br.hasErrors()) {
            String message = "Data contains errors\n" + getErrorMessages(br);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, message);
        }

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getEmail(), authenticationRequest.getPassword()));
        } catch (DisabledException | BadCredentialsException | LockedException e) {
             log.warn(String.format("Login attempt failed for user %s: %s", authenticationRequest.getEmail(), e.getMessage()));
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
        final UserDetails userDetails = userService.loadUserByUsername(authenticationRequest.getEmail());
        final String token = jwtTokenUtil.generateToken(userDetails);
        final UserDto userDto = userService.findByEmail(authenticationRequest.getEmail());
        final String role = Utils.getMaximumRole(new HashSet<>(userDetails.getAuthorities()));
        log.info("Login Successful for " + authenticationRequest.getEmail());
        return ResponseEntity.ok(AuthenticationResponse.builder()
                .id(userDto.getId())
                .email(userDetails.getUsername())
                .role(role)
                .token(token)
                .build());
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody @Valid RegistrationRequest registrationRequest,
                                      BindingResult br,
                                      HttpServletRequest request) {
        if (br.hasErrors()) {
            String message = "Data contains errors\n" + getErrorMessages(br);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, message);
        }

        if (userService.existsByEmail(registrationRequest.getEmail())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Email already registered");
        }

        try {
            UserDto user = UserDto.fromRegistrationRequest(registrationRequest);
            log.info(String.format("Registration request for user %s", user.toString()));
            userService.insertUser(user);
            String token = verificationTokenService.insertToken(user);

            String confirmationUrl = request.getScheme() +
                    "://" + request.getServerName() +
                    ":" + request.getServerPort() +
                    "/api/auth/confirm/" + token;

            SimpleMailMessage mail = new SimpleMailMessage();
            mail.setTo(user.getUsername());
            mail.setSubject("Registration confirm");
            mail.setText("To confirm your email address, please click on the link below:\n" +
                    confirmationUrl);
            mail.setFrom("noreply@pedibus.com");
            emailService.sendEmail(mail);
        } catch (UserNotFoundException | RoleNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok().build();
    }

    @GetMapping("/confirm/{randomUUID}")
    public ResponseEntity<?> activateUser(@PathVariable String randomUUID) {
        try {
            final VerificationTokenDto token = verificationTokenService.loadToken(randomUUID);
            if (token.getExpiringDate().before(new Date())) {
                verificationTokenService.deleteToken(token);
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Token expired");
            }

            userService.activateUser(token.getUser());
            verificationTokenService.deleteToken(token);
        } catch (VerificationTokenNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Token not found");
        }

        return ResponseEntity.ok().build();
    }

    @PostMapping("/recover")
    public ResponseEntity<?> recover(@RequestBody @Valid RecoverPasswordRequest recoverPasswordRequest,
                                     BindingResult br,
                                     HttpServletRequest request) {
        if (br.hasErrors()) {
            String message = "Data contains errors\n" + getErrorMessages(br);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, message);
        }

        try {
            String token = passwordResetTokenService.insertToken(recoverPasswordRequest.getEmail());

            String confirmationUrl = request.getScheme() +
                    "://" + request.getServerName() +
                    ":" + request.getServerPort() +
                    "/api/auth/recover/" + token;

            SimpleMailMessage mail = new SimpleMailMessage();
            mail.setTo(recoverPasswordRequest.getEmail());
            mail.setSubject("Reset password");
            mail.setText("To reset your password, please click on the link below:\n" +
                    confirmationUrl);
            mail.setFrom("noreply@pedibus.com");
            emailService.sendEmail(mail);
        } catch (UserNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
        } catch (UserNotEnabledException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not enabled");
        }

        return ResponseEntity.ok().build();
    }

    @PostMapping("/recover/{randomUUID}")
    public ResponseEntity<?> recover(@PathVariable String randomUUID,
                                     @RequestBody @Valid ResetPasswordRequest resetPasswordRequest,
                                     BindingResult br) {
        if (br.hasErrors()) {
            String message = "Data contain errors\n" + getErrorMessages(br);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, message);
        }

        try {
            final PasswordResetTokenDto token = passwordResetTokenService.loadToken(randomUUID);
            if (token.getExpiringDate().before(new Date())) {
                passwordResetTokenService.deleteToken(token);
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Token expired");
            }

            UserDto user = token.getUser();
            user.setPassword(resetPasswordRequest.getPassword());
            userService.updatePassword(user);
            passwordResetTokenService.deleteToken(token);
        } catch (PasswordResetTokenNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Token not found");
        }

        return ResponseEntity.ok().build();
    }

    private String getErrorMessages(BindingResult br) {
        StringBuilder sb = new StringBuilder();
        for (FieldError fe : br.getFieldErrors()) {
            sb.append(fe.getField())
                    .append(" ")
                    .append(fe.getDefaultMessage());
        }
        return sb.toString();
    }
}

package it.polito.ai.pedibus.controllers.viewmodels.validation;

public interface PasswordsMatcher {

    String getPassword();
    String getPasswordConfirm();
}

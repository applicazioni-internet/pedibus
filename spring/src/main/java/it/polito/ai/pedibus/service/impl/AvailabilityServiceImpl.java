package it.polito.ai.pedibus.service.impl;

import it.polito.ai.pedibus.exceptions.AvailabilityNotFoundException;
import it.polito.ai.pedibus.exceptions.LineNotFoundException;
import it.polito.ai.pedibus.exceptions.UserNotFoundException;
import it.polito.ai.pedibus.model.entities.AvailabilityEntity;
import it.polito.ai.pedibus.model.entities.LineEntity;
import it.polito.ai.pedibus.model.entities.UserEntity;
import it.polito.ai.pedibus.model.repositories.AvailabilityRepository;
import it.polito.ai.pedibus.model.repositories.LineRepository;
import it.polito.ai.pedibus.model.repositories.UserRepository;
import it.polito.ai.pedibus.service.AvailabilityService;
import it.polito.ai.pedibus.service.dtos.AvailabilityDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AvailabilityServiceImpl implements AvailabilityService {

    private final AvailabilityRepository availabilities;
    private final UserRepository users;
    private final LineRepository lines;

    @Autowired
    public AvailabilityServiceImpl(AvailabilityRepository availabilities,
                                   UserRepository users,
                                   LineRepository lines) {
        this.availabilities = availabilities;
        this.users = users;
        this.lines = lines;
    }

    @Override
    public AvailabilityDto loadById(Long id) throws AvailabilityNotFoundException {
        AvailabilityEntity availability = availabilities.findById(id).orElseThrow(AvailabilityNotFoundException::new);
        return AvailabilityDto.fromEntity(availability);
    }

    @Override
    public List<AvailabilityDto> loadAvailabilitiesByDateAndLine(LocalDate date, Long lineId) {
        return availabilities.findAllByDateAndLine_Id(date, lineId).stream()
                .map(AvailabilityDto::fromEntity)
                .collect(Collectors.toList());
    }

    @Override
    public List<AvailabilityDto> loadAvailabilitiesByDateAndUser(LocalDate date, Long userId) {
        return availabilities.findAllByDateAndUserId(date, userId).stream()
                .map(AvailabilityDto::fromEntity)
                .collect(Collectors.toList());
    }

    @Override
    public AvailabilityDto insertAvailability(AvailabilityDto proposal) throws UserNotFoundException, LineNotFoundException {
        Optional<AvailabilityEntity> foundAvailability = availabilities.findByDateAndLine_IdAndUser_Email(proposal.getDate(),proposal.getLineId(), proposal.getUser());

        if (foundAvailability.isPresent())
            return AvailabilityDto.fromEntity(foundAvailability.get());

        UserEntity user = users.findByEmail(proposal.getUser())
                .orElseThrow(UserNotFoundException::new);
        LineEntity line = lines.findById(proposal.getLineId())
                .orElseThrow(LineNotFoundException::new);
        AvailabilityEntity availability = availabilities.save(AvailabilityEntity.fromProposal(proposal, line, user));

        return AvailabilityDto.fromEntity(availability);
    }

    @Override
    public void deleteAvailabilityById(long id) throws AvailabilityNotFoundException {
        if (! availabilities.existsById(id))
            throw new AvailabilityNotFoundException();
        availabilities.deleteById(id);
    }

    @Override
    public List<AvailabilityDto> loadByUserId(long id) {
        return availabilities.findAllByUser_Id(id).stream()
                .map(AvailabilityDto::fromEntity)
                .collect(Collectors.toList());
    }

    @Override
    public AvailabilityDto approveAvailability(long id) throws AvailabilityNotFoundException {
        AvailabilityEntity entity = availabilities.findById(id)
                .orElseThrow(AvailabilityNotFoundException::new);
        entity.setApproved(true);
        return AvailabilityDto.fromEntity(availabilities.save(entity));
    }
}

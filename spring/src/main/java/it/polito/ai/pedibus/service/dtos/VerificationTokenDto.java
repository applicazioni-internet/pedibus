package it.polito.ai.pedibus.service.dtos;

import it.polito.ai.pedibus.model.entities.VerificationTokenEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

/**
 * Verification token DTO
 */
@Data
@Builder
@AllArgsConstructor
public class VerificationTokenDto {

    private String token;
    private Date expiringDate;
    private UserDto user;

    /**
     * Returns a {@link VerificationTokenDto} from a {@link VerificationTokenEntity}
     * @param entity    the entity to convert
     * @return          the created dto.
     */
    public static VerificationTokenDto fromEntity(VerificationTokenEntity entity) {
        final UserDto user = UserDto.fromEntity(entity.getUser());
        return VerificationTokenDto.builder()
                .expiringDate(entity.getExpiringDate())
                .token(entity.getToken())
                .user(user)
                .build();
    }
}

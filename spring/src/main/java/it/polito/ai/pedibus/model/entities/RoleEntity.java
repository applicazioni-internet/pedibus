package it.polito.ai.pedibus.model.entities;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "roles")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RoleEntity {

    @Id
    @Column(updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "role_generator")
    @SequenceGenerator(
            name = "role_generator",
            sequenceName = "role_sequence",
            allocationSize = 1
    )
    @ToString.Exclude
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;
}

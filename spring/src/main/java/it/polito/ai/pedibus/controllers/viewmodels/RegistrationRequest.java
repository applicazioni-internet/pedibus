package it.polito.ai.pedibus.controllers.viewmodels;

import it.polito.ai.pedibus.controllers.viewmodels.validation.PasswordsMatch;
import it.polito.ai.pedibus.controllers.viewmodels.validation.PasswordsMatcher;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;

import java.time.LocalDate;

import static it.polito.ai.pedibus.controllers.viewmodels.validation.ValidationConstants.*;

@Data
@PasswordsMatch
public class RegistrationRequest implements PasswordsMatcher {

    @Email
    @NotNull
    private String email;

    @NotNull
    @Size(min = MIN_NAME_LENGTH, max = MAX_NAME_LENGTH)
    private String firstName;

    @NotNull
    @Size(min = MIN_NAME_LENGTH, max = MAX_NAME_LENGTH)
    private String lastName;

    @NotNull
    @Size(min = MIN_PASSWORD_LENGTH, max = MAX_PASSWORD_LENGTH)
    @Pattern(regexp = PASSWORD_REGEX)
    private String password;

    @NotNull
    @Size(min = MIN_PASSWORD_LENGTH, max = MAX_PASSWORD_LENGTH)
    private String passwordConfirm;

    @NotEmpty
    private Child[] children;

    @Data
    public static class Child {

        @NotNull
        @Size(min = MIN_NAME_LENGTH, max = MAX_NAME_LENGTH)
        private String firstName;

        @NotNull
        @Size(min = MIN_NAME_LENGTH, max = MAX_NAME_LENGTH)
        private String lastName;

        @NotNull
        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
        @Past
        private LocalDate birthDate;
    }
}

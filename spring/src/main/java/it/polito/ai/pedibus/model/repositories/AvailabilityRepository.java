package it.polito.ai.pedibus.model.repositories;

import it.polito.ai.pedibus.model.entities.AvailabilityEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface AvailabilityRepository extends JpaRepository<AvailabilityEntity, Long> {

    Optional<AvailabilityEntity> findById(Long id);

    List<AvailabilityEntity> findAllByDateAndLine_Id(LocalDate date, Long lineId);

    List<AvailabilityEntity> findAllByDateAndUserId(LocalDate date, Long userId);

    Optional<AvailabilityEntity> findByDateAndLine_IdAndUser_Email(LocalDate date, Long lineId, String email);

    List<AvailabilityEntity> findAllByUser_Id(long userId);
}

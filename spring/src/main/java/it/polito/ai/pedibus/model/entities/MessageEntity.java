package it.polito.ai.pedibus.model.entities;

import it.polito.ai.pedibus.service.dtos.MessageDto;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "message")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MessageEntity {

    @Id
    @Column(updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "message_generator")
    @SequenceGenerator(
            name = "message_generator",
            sequenceName = "message_sequence",
            allocationSize = 1)
    private Long id;

    @ManyToOne
    private UserEntity sender;

    @ManyToOne
    private UserEntity recipient;

    @Lob
    private String body;

    @Column(nullable = false)
    private LocalDateTime date;

    @Column(nullable = false)
    private Boolean read;

    public static MessageEntity fromDto(MessageDto dto, UserEntity s, UserEntity r) {
        return MessageEntity.builder()
                .sender(s)
                .recipient(r)
                .body(dto.getBody())
                .date(dto.getDate())
                .read(false)
                .build();
    }
}

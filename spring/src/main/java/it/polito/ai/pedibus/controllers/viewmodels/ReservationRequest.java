package it.polito.ai.pedibus.controllers.viewmodels;

import it.polito.ai.pedibus.service.dtos.ReservationDTO;
import it.polito.ai.pedibus.service.dtos.StopDTO;

import it.polito.ai.pedibus.service.dtos.ChildDTO;
import lombok.Builder;
import lombok.Value;

import java.time.LocalDate;

@Value
@Builder
public class ReservationRequest {
    Long departureId;
    Long arrivalId;
    Boolean presence;
    Long childId;

    public static ReservationDTO toDTO(final ChildDTO child,
                                       final LocalDate date,
                                       final StopDTO departure,
                                       final StopDTO arrival,
                                       final Boolean presence) {
        return ReservationDTO.builder()
                .date(date)
                .departure(departure)
                .arrival(arrival)
                .child(child)
                .present(presence)
                .build();
    }

    public ReservationDTO toDto(final ChildDTO child,
                                final LocalDate date,
                                final StopDTO departure,
                                final StopDTO arrival,
                                final Boolean presence) {
        return toDTO(child, date, departure, arrival, presence);
    }

}

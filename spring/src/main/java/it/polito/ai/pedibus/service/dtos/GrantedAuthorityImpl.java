package it.polito.ai.pedibus.service.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

@AllArgsConstructor
@Data
public class GrantedAuthorityImpl implements GrantedAuthority {

    private String authority;
}

package it.polito.ai.pedibus.controllers.viewmodels;

import it.polito.ai.pedibus.service.dtos.AvailabilityDto;
import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Builder
@Data
public class Availability {

    private long id;
    private long line;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate date;
    private String user;
    private boolean approved;

    public static Availability fromDto(AvailabilityDto dto) {
        return Availability.builder()
                .approved(dto.isApproved())
                .date(dto.getDate())
                .id(dto.getId())
                .line(dto.getLineId())
                .user(dto.getUser())
                // TODO
//                .line(dto.getLine())
//                .user(dto.getUser())
                .build();
    }
}

package it.polito.ai.pedibus.service;

import it.polito.ai.pedibus.service.dtos.ChildDTO;
import it.polito.ai.pedibus.exceptions.ChildNotFoundException;

public interface ChildService {

	ChildDTO getChildById(Long id) throws ChildNotFoundException;

	boolean existsById(Long id);
}

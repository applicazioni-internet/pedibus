package it.polito.ai.pedibus.service;

import it.polito.ai.pedibus.exceptions.UserNotEnabledException;
import it.polito.ai.pedibus.exceptions.UserNotFoundException;
import it.polito.ai.pedibus.service.dtos.UserDto;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

/**
 *  Utility service for managing users.
 */
public interface UserService extends UserDetailsService {

    /**
     * Returns a boolean that indicates if the {@link String} email is already registered.
     * @param email the email to check
     * @return      the boolean answer.
     */
    boolean existsByEmail(String email);

    /**
     * Inserts a new {@link UserDto} user in the database.
     * @param dto   the user to insert
     */
    void insertUser(UserDto dto);

    /**
     * Activates the {@link UserDto} user.
     * @param dto   the user to activate
     */
    void activateUser(UserDto dto);

    /**
     * Updates the password of the {@link UserDto} user.
     * @param dto   the user to update
     */
    void updatePassword(UserDto dto);

    /**
     * Returns all the users
     * @return  the list of users
     */
    List<UserDto> getAllUsers();

    /**
     * Returns a {@link UserDto} object that identifies the user.
     * <p>The id argument must specify the id of the user to retrieve.</p>
     * @param id    the id of the user
     * @return      the retrieved user
     * @throws UserNotFoundException    If the user cannot be found
     */
    UserDto loadUserById(long id) throws UserNotFoundException;

    /**
     * Returns a {@link UserDto} object given the email.
     * @param email the email of the user
     * @return the retrieved user
     */
   UserDto findByEmail(String email);

    /**
     * Returns a {@link UserDto} object parent of a child.
     * @param childId   the id of the child
     * @return the retrieved user
     */
    UserDto getUserByChildId(Long childId);

    /**
     * Change the user role of a {@link UserDto}.
     * @param id        the id of the user
     * @param promote   if true, the user will be promoted to admin
     *                  if false, the user will be downgraded to a simple user
     * @return      the updated user
     * @throws UserNotFoundException    If the user cannot be found
     */
    UserDto promoteUserToAdmin(long id, boolean promote) throws UserNotFoundException;
}

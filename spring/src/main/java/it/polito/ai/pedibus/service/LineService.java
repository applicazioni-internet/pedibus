package it.polito.ai.pedibus.service;

import it.polito.ai.pedibus.service.dtos.LineDTO;
import it.polito.ai.pedibus.exceptions.LineNotFoundException;
import it.polito.ai.pedibus.exceptions.UserNotFoundException;

import java.util.List;

public interface LineService {

	List<LineDTO> getAllLines();

	LineDTO getLineById(Long id) throws LineNotFoundException;

	boolean existsById(Long id);

    LineDTO addCompanionToLine(long lineId, long userId) throws LineNotFoundException, UserNotFoundException;

	LineDTO deleteCompanion(Long lineId, Long userId) throws LineNotFoundException, UserNotFoundException;
}

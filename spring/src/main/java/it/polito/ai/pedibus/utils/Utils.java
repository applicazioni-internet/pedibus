package it.polito.ai.pedibus.utils;

import it.polito.ai.pedibus.controllers.viewmodels.*;
import it.polito.ai.pedibus.model.entities.*;
import it.polito.ai.pedibus.service.dtos.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static it.polito.ai.pedibus.utils.Roles.*;

@Slf4j
public class Utils {

    public static LineDTO toDTO(LineEntity entity) {
        LineDTO dto = LineDTO.builder()
                .id(entity.getId())
                .name(entity.getName())
                .companions(entity.getCompanions().stream()
                        .map(Utils::toDTO)
                        .collect(Collectors.toSet()))
                .build();

        List<StopDTO> forwardStops = entity.getStops().stream()
                .filter(s -> s.getDirection().equals(Direction.FORWARD))
                .sorted()
                .map(s -> StopDTO.fromEntity(dto, s))
                .collect(Collectors.toList());
        List<StopDTO> backwardStops = entity.getStops().stream()
                .filter(s -> s.getDirection().equals(Direction.BACKWARD))
                .sorted()
                .map(s -> StopDTO.fromEntity(dto, s))
                .collect(Collectors.toList());

        dto.setBackwards(backwardStops);
        dto.setForwards(forwardStops);

        return dto;
    }

    public static LineVM toVM(LineDTO dto) {
        return LineVM.builder()
                .id(dto.getId())
                .companions(dto.getCompanions().stream()
                        .map(Utils::toVM)
                        .map(UserVM::getEmail)
                        .collect(Collectors.toSet()))
                .name(dto.getName())
                .backward(dto.getBackwards().stream()
                        .sorted()
                        .map(StopVM::fromDto)
                        .collect(Collectors.toList()))
                .forward(dto.getForwards().stream()
                        .sorted()
                        .map(StopVM::fromDto)
                        .collect(Collectors.toList()))
                .build();
    }

    public static UserVM toVM(UserDto user) {
        final String role = getMaximumRole(user.getAuthorities());

        return UserVM.builder()
                .id(user.getId())
                .email(user.getUsername())
                .firstName(user.getFirstName())
                .lastName(user.getLastName()).enabled(user.isEnabled())
                .children(user.getChildren().stream()
                        .map(ChildVM::fromDto)
                        .sorted(Comparator.comparing(ChildVM::getLastName)
                                .thenComparing(ChildVM::getFirstName))
                        .collect(Collectors.toList()))
                .companionLines(user.getCompanionLines())
                .role(role)
                .build();
    }

    public static UserDto toDTO(UserEntity user) {
        return UserDto.fromEntity(user);
    }

    public static ChildDTO toDTO(ChildEntity entity, List<ReservationDTO> reservations, UserDto parent) {
        return ChildDTO.builder()
                .id(entity.getId())
                .firstName(entity.getFirstName())
                .lastName(entity.getLastName())
                .birthDate(entity.getBirthDate())
                .parent(parent)
                .reservations(reservations)
                .build();
    }

    public static ReservationVM toVM(ReservationDTO r) {
        return ReservationVM.builder()
                .id(r.getId())
                .date(r.getDate())
                .child(ReservationVM.Child.builder()
                        .id(r.getChild().getId())
                        .name(r.getChild().getFirstName() + " " + r.getChild().getLastName())
                        .birthDate(r.getChild().getBirthDate())
                        .build())
                .departure(StopVM.fromDto(r.getDeparture()))
                .arrival(StopVM.fromDto(r.getArrival()))
                .line(r.getDeparture().getLine().getId())
                .presence(r.isPresent())
                .build();
    }

//    public static ReservationVM toVM(ReservationDTO r, Long lineId) {
//        return ReservationVM.builder()
//                .id(r.getId())
//                .date(r.getDate())
//                .child(Utils.toVM(r.getChild()))
//                .departure(Utils.toVM(r.getDeparture()))
//                .arrival(Utils.toVM(r.getArrival()))
//                .line(lineId)
//                .build();
//    }

    public static ReservationDTO toDTOInsert(ReservationEntity r,
                                             List<ReservationDTO> reservations,
                                             UserDto parent,
                                             LineDTO line) {
        ChildEntity c = r.getChild();
        log.info("Child taken: " + c.toString());
        log.info("Child parent: " + c.getParent());
        log.info("Parent taken: " + parent.toString());

        return ReservationDTO.builder()
                .id(r.getId())
                .date(r.getDate())
                .child(Utils.toDTO(c, reservations, parent))
                .departure(StopDTO.fromEntity(line, r.getDeparture()))
                .arrival(StopDTO.fromEntity(line, r.getArrival()))
                .present(r.isPresence())
                .build();
    }

    public static ReservationEntity toEntity(ReservationDTO r) {
		return ReservationEntity.builder()
                .date(r.getDate())
                .departure(StopEntity.fromDto(r.getDeparture()))
                .arrival(StopEntity.fromDto(r.getArrival()))
                .child(ChildEntity.fromDto(r.getChild()))
                .presence(r.isPresent())
				.build();
    }

    public static String getMaximumRole(Set<GrantedAuthority> authorities) {
        final String role;
        if (authorities.contains(new GrantedAuthorityImpl(ROLE_SUPER_ADMINISTRATOR))) {
            role = ROLE_SUPER_ADMINISTRATOR;
        } else if (authorities.contains(new GrantedAuthorityImpl(ROLE_ADMINISTRATOR))) {
            role = ROLE_ADMINISTRATOR;
        } else {
            role = ROLE_USER;
        }
        return role;
    }
}

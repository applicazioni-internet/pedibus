package it.polito.ai.pedibus.model.entities;

import it.polito.ai.pedibus.service.dtos.ChildDTO;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "child")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChildEntity {

    @Id
    @Column(updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "child_generator")
    @SequenceGenerator(
            name = "child_generator",
            sequenceName = "child_sequence",
            allocationSize = 1
    )
    private Long id;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false)
    private LocalDate birthDate;

    @ManyToOne
    @JoinColumn(
            name = "parent_id",
            nullable = false
    )
    @ToString.Exclude
    private UserEntity parent;

    public static ChildEntity fromDto(ChildDTO dto) {
        return ChildEntity.builder()
                .id(dto.getId())
                .firstName(dto.getFirstName())
                .lastName(dto.getLastName())
                .birthDate(dto.getBirthDate())
                .build();
    }
}

package it.polito.ai.pedibus.model.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "password_reset_token")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PasswordResetTokenEntity {

    public static final long MILLISECONDS_VALIDITY = 24 * 60 * 60 * 1000; // 1 day

    @Id
    @Column(updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "password_reset_token_generator")
    @SequenceGenerator(
            name = "password_reset_token_generator",
            sequenceName = "password_reset_token_sequence",
            allocationSize = 1
    )
    private Long id;

    @Column
    private String token;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiringDate;

    @OneToOne(targetEntity = UserEntity.class)
    @JoinColumn(nullable = false, name = "user_id")
    private UserEntity user;
}

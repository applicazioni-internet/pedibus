package it.polito.ai.pedibus.controllers.viewmodels;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UserPromotionRequest {

    @NotNull
    private Boolean promote;
}

package it.polito.ai.pedibus.service.dtos;

import it.polito.ai.pedibus.controllers.viewmodels.AvailabilityProposal;
import it.polito.ai.pedibus.model.entities.AvailabilityEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

/**
 * Availability DTO
 */
@Data
@Builder
@AllArgsConstructor
public class AvailabilityDto {

    private Long id;
    private Long lineId;
    private LocalDate date;
    private String user;
    private boolean approved;

    /**
     * Returns a {@link AvailabilityDto} from a {@link AvailabilityEntity}
     * @param entity    the entity to convert
     * @return          the created dto.
     */
    public static AvailabilityDto fromEntity(AvailabilityEntity entity) {
        return AvailabilityDto.builder()
                .id(entity.getId())
                .lineId(entity.getLine().getId())
                .date(entity.getDate())
                .approved(entity.getApproved())
                .user(entity.getUser().getEmail())
                .build();
    }

    /**
     * Returns a {@link AvailabilityDto} from a proposal
     * @param proposal  the proposal to convert
     * @param username  the user issuing the proposal
     * @return          the created dto.
     */
    public static AvailabilityDto fromAvailabilityProposal(AvailabilityProposal proposal, String username) {
        return AvailabilityDto.builder()
                .approved(false)
                .date(proposal.getDate())
                .lineId(proposal.getLineId())
                .user(username)
                .build();
    }
}

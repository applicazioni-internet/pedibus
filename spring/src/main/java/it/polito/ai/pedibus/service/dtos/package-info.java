/**
 * The DTO objects managed by services.
 */
package it.polito.ai.pedibus.service.dtos;
package it.polito.ai.pedibus.controllers.viewmodels;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class MessageUpdate {

    @NotNull
    private Boolean read;
}

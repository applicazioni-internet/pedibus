package it.polito.ai.pedibus.service.impl;

import it.polito.ai.pedibus.exceptions.MessageNotFoundException;
import it.polito.ai.pedibus.exceptions.UserNotFoundException;
import it.polito.ai.pedibus.model.entities.MessageEntity;
import it.polito.ai.pedibus.model.entities.UserEntity;
import it.polito.ai.pedibus.model.repositories.MessageRepository;
import it.polito.ai.pedibus.model.repositories.UserRepository;
import it.polito.ai.pedibus.service.MessageService;
import it.polito.ai.pedibus.service.dtos.MessageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class MessageServiceImpl implements MessageService {

    private final MessageRepository messages;
    private final UserRepository users;

    @Autowired
    public MessageServiceImpl(MessageRepository messages, UserRepository users) {
        this.messages = messages;
        this.users = users;
    }

    @Override
    public MessageDto getMessageById(long id) throws MessageNotFoundException {
        final MessageEntity entity = messages.findById(id).orElseThrow(MessageNotFoundException::new);
        return MessageDto.fromEntity(entity);
    }

    @Override
    public MessageDto sendMessage(MessageDto message) throws UserNotFoundException {
        final UserEntity s = users.findByEmail(message.getSender()).orElseThrow(UserNotFoundException::new);
        final UserEntity r = users.findByEmail(message.getRecipient()).orElseThrow(UserNotFoundException::new);
        MessageEntity e = MessageEntity.fromDto(message, s, r);
        MessageEntity inserted = messages.save(e);
        return MessageDto.fromEntity(inserted);
    }

    @Override
    public List<MessageDto> getMessagesBySenderId(long id) {
        return messages.findAllBySenderId(id).stream()
                .map(MessageDto::fromEntity)
                .collect(Collectors.toList());
    }

    @Override
    public List<MessageDto> getMessagesByRecipientId(long id) {
        return messages.findAllByRecipientId(id).stream()
                .map(MessageDto::fromEntity)
                .collect(Collectors.toList());
    }

    @Override
    public MessageDto updateMessage(long id, boolean read) throws MessageNotFoundException {
        MessageEntity message = messages.findById(id).orElseThrow(MessageNotFoundException::new);
        message.setRead(read);
        messages.save(message);
        return MessageDto.fromEntity(message);
    }

}

package it.polito.ai.pedibus.controllers.viewmodels;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Builder
@Data
public class AuthenticationResponse implements Serializable {

    private Long id;
    private String email;
    private String role;
    private String token;
}

package it.polito.ai.pedibus.service.impl;

import it.polito.ai.pedibus.exceptions.LineNotFoundException;
import it.polito.ai.pedibus.exceptions.UserNotFoundException;
import it.polito.ai.pedibus.model.entities.LineEntity;
import it.polito.ai.pedibus.model.entities.UserEntity;
import it.polito.ai.pedibus.model.repositories.LineRepository;
import it.polito.ai.pedibus.model.repositories.UserRepository;
import it.polito.ai.pedibus.service.LineService;
import it.polito.ai.pedibus.service.dtos.LineDTO;
import it.polito.ai.pedibus.utils.Utils;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LineServiceImpl implements LineService {

    private final LineRepository lineRepository;
    private final UserRepository users;

    @Autowired
    public LineServiceImpl(LineRepository lineRepository, UserRepository users) {
        this.lineRepository = lineRepository;
        this.users = users;
    }

    @Override
    public List<LineDTO> getAllLines() {
        return lineRepository.findAll().stream().map(Utils::toDTO).collect(Collectors.toList());
    }

    @Override
    public LineDTO getLineById(Long id) throws LineNotFoundException {
        return lineRepository.findById(id).map(Utils::toDTO).orElseThrow(LineNotFoundException::new);
    }

    @Override
    public boolean existsById(Long id) {
        return lineRepository.existsById(id);
    }

    @Override
    public LineDTO addCompanionToLine(long lineId, long userId) throws LineNotFoundException, UserNotFoundException {
        LineEntity line = lineRepository.findById(lineId).orElseThrow(LineNotFoundException::new);
        UserEntity user = users.findById(userId).orElseThrow(UserNotFoundException::new);
        line.getCompanions().add(user);
        return Utils.toDTO(lineRepository.save(line));
    }

    @Override
    public LineDTO deleteCompanion(Long lineId, Long userId) throws LineNotFoundException, UserNotFoundException {
        LineEntity line = lineRepository.findById(lineId).orElseThrow(LineNotFoundException::new);
        UserEntity user = users.findById(userId).orElseThrow(UserNotFoundException::new);
        line.getCompanions().remove(user);
        return Utils.toDTO(lineRepository.save(line));
    }
}

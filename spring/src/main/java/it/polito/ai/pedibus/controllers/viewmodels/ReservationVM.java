package it.polito.ai.pedibus.controllers.viewmodels;

import it.polito.ai.pedibus.service.dtos.ReservationDTO;
import it.polito.ai.pedibus.utils.Utils;
import lombok.Builder;
import lombok.Value;

import java.time.LocalDate;

@Value
@Builder
public class ReservationVM {

	Long id;
    LocalDate date;
    Child child;
    Long line;
    StopVM departure;
    StopVM arrival;
    boolean presence;

    @Value
    @Builder
    public static class Child {
        Long id;
        String name;
        LocalDate birthDate;
    }

    public static ReservationVM fromDto(ReservationDTO dto) {
        return Utils.toVM(dto);
    }
}

package it.polito.ai.pedibus.service.impl;

import it.polito.ai.pedibus.exceptions.RoleNotFoundException;
import it.polito.ai.pedibus.exceptions.UserNotFoundException;
import it.polito.ai.pedibus.model.entities.ChildEntity;
import it.polito.ai.pedibus.model.entities.RoleEntity;
import it.polito.ai.pedibus.model.entities.UserEntity;
import it.polito.ai.pedibus.model.repositories.ChildRepository;
import it.polito.ai.pedibus.model.repositories.RoleRepository;
import it.polito.ai.pedibus.model.repositories.UserRepository;
import it.polito.ai.pedibus.service.UserService;
import it.polito.ai.pedibus.service.dtos.UserDto;
import it.polito.ai.pedibus.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static it.polito.ai.pedibus.utils.Roles.ROLE_ADMINISTRATOR;
import static it.polito.ai.pedibus.utils.Roles.ROLE_USER;

/**
 * The implementation of {@link UserService}
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    private final UserRepository users;
    private final RoleRepository roles;
    private final ChildRepository children;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository users,
                           RoleRepository roles,
                           ChildRepository children,
                           PasswordEncoder passwordEncoder) {
        this.users = users;
        this.roles = roles;
        this.children = children;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity user = users.findByEmail(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));
        return UserDto.fromEntity(user);
    }

    @Override
    public boolean existsByEmail(String email) {
        return users.existsByEmail(email);
    }

    @Override
    public void insertUser(UserDto dto) throws RoleNotFoundException {
        RoleEntity role = roles.findByName(ROLE_USER)
                .orElseThrow(RoleNotFoundException::new);
        UserEntity entity = UserEntity.fromDto(dto, role, passwordEncoder);
        users.save(entity);
        children.saveAll(entity.getChildren());
    }

    @Override
    public void activateUser(UserDto dto) throws UserNotFoundException {
        UserEntity entity = users.findByEmail(dto.getUsername())
                .orElseThrow(UserNotFoundException::new);
        entity.setEnabled(true);
        users.save(entity);
    }

    @Override
    public void updatePassword(UserDto dto) throws UserNotFoundException {
        UserEntity entity = users.findByEmail(dto.getUsername())
                .orElseThrow(UserNotFoundException::new);
        entity.setPassword(passwordEncoder.encode(dto.getPassword()));
        users.save(entity);
    }

    @Override
    public List<UserDto> getAllUsers() {
        return users.findAll().stream()
                .map(UserDto::fromEntity)
                .collect(Collectors.toList());
    }

    @Override
    public UserDto loadUserById(long id) throws UserNotFoundException {
        return users.findById(id).map(UserDto::fromEntity)
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public UserDto findByEmail(String email) {
        return users.findByEmail(email).map(UserDto::fromEntity)
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public UserDto getUserByChildId(Long childId) {
        System.out.println("=== Getting Child By ChildId ===");
        Optional<ChildEntity> c = children.findById(childId);

        System.out.println("=== Getting User By Child ===");
        Optional<UserEntity> u = users.findByChildren(c.get());

        System.out.println("=== Got User By ChildId => " + u.get().getEmail() + " ===");
        return u.map(UserDto::fromEntity).orElseThrow(UserNotFoundException::new);
                //findByEmail(email).map(UserDto::fromEntity).orElseThrow(UserNotFoundException::new);
    }

    @Override
    public UserDto promoteUserToAdmin(long id, boolean promote) throws UserNotFoundException {
        UserEntity user = users.findById(id)
                .orElseThrow(UserNotFoundException::new);
        RoleEntity role = roles.findByName(ROLE_ADMINISTRATOR)
                .orElseThrow(() -> { throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Role not found"); });
        if (promote) {
            user.getRoles().add(role);
        } else {
            user.getRoles().remove(role);
        }
        user = users.save(user);
        return Utils.toDTO(user);
    }
}

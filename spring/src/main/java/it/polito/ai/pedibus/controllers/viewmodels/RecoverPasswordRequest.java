package it.polito.ai.pedibus.controllers.viewmodels;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RecoverPasswordRequest {

	@NotNull
	@Email
	private String email;
}

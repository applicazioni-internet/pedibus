package it.polito.ai.pedibus.service;

import it.polito.ai.pedibus.exceptions.StopNotFoundException;
import it.polito.ai.pedibus.service.dtos.StopDTO;

public interface StopService {

	StopDTO getStopById(Long id) throws StopNotFoundException;

	boolean existsById(Long id);
}

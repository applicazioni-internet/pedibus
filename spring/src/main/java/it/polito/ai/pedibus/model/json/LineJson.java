package it.polito.ai.pedibus.model.json;

import lombok.Data;

import java.util.List;

@Data
public class LineJson {
    private List<StopJson> stops;
}

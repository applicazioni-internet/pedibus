package it.polito.ai.pedibus.controllers;

import it.polito.ai.pedibus.controllers.viewmodels.LineVM;
import it.polito.ai.pedibus.exceptions.LineNotFoundException;
import it.polito.ai.pedibus.exceptions.UserNotFoundException;
import it.polito.ai.pedibus.service.LineService;
import it.polito.ai.pedibus.service.dtos.LineDTO;
import it.polito.ai.pedibus.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.stream.Collectors;

import static it.polito.ai.pedibus.utils.Roles.ROLE_ADMINISTRATOR;
import static it.polito.ai.pedibus.utils.Roles.ROLE_SUPER_ADMINISTRATOR;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/api/lines")
public class LineController {

	private final LineService lineService;

	@Autowired
	public LineController(LineService lineService) {
		this.lineService = lineService;
	}

	@GetMapping("")
	public Resources<Resource<LineVM>> getAllLines() {
		return new Resources<>(lineService.getAllLines().stream()
				.map(Utils::toVM)
				.sorted()
				.map(l -> new Resource<>(l,
						// Generate link for a single line
						linkTo(methodOn(LineController.class)
								.getLineById(l.getId()))
								.withSelfRel()))
				.collect(Collectors.toList()),
				// Generate link for the collection
				linkTo(methodOn(LineController.class)
						.getAllLines())
						.withSelfRel());
	}

	@GetMapping("/{id}")
	public Resource<LineVM> getLineById(@PathVariable Long id) {
		try {
			LineDTO lineDTO = lineService.getLineById(id);
			return new Resource<>(Utils.toVM(lineDTO),
					linkTo(methodOn(LineController.class)
							.getLineById(id))
							.withSelfRel(),
					linkTo(methodOn(LineController.class)
							.getAllLines())
							.withRel("lines"));
		} catch (LineNotFoundException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Line not found");
		}
	}

	@PostMapping("/{lineId}/companions/{userId}")
	@Secured({ROLE_ADMINISTRATOR, ROLE_SUPER_ADMINISTRATOR})
	@ResponseStatus(HttpStatus.ACCEPTED)
	public Resource<LineVM> addCompanionToLine(@PathVariable Long lineId,
											   @PathVariable Long userId) {
		try {
			LineDTO lineDto = lineService.addCompanionToLine(lineId, userId);
			return new Resource<>(Utils.toVM(lineDto),
					linkTo(methodOn(LineController.class)
							.getLineById(lineId))
							.withSelfRel(),
					linkTo(methodOn(LineController.class)
							.getAllLines())
							.withRel("lines"));
		} catch (LineNotFoundException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Line not found");
		} catch (UserNotFoundException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
		}
	}

	@DeleteMapping("/{lineId}/companions/{userId}")
	@Secured({ROLE_ADMINISTRATOR, ROLE_SUPER_ADMINISTRATOR})
	@ResponseStatus(HttpStatus.ACCEPTED)
	public Resource<LineVM> deleteCompanionOfLine(@PathVariable Long lineId,
												  @PathVariable Long userId) {
		try {
			LineDTO lineDto = lineService.deleteCompanion(lineId, userId);
			return new Resource<>(Utils.toVM(lineDto),
					linkTo(methodOn(LineController.class)
							.getLineById(lineId))
							.withSelfRel(),
					linkTo(methodOn(LineController.class)
							.getAllLines())
							.withRel("lines"));
		} catch (LineNotFoundException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Line not found");
		} catch (UserNotFoundException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
		}
	}
}
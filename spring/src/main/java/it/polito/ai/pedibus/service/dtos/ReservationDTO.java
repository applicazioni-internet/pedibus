package it.polito.ai.pedibus.service.dtos;

import it.polito.ai.pedibus.model.entities.ReservationEntity;
import it.polito.ai.pedibus.utils.Utils;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.time.LocalDate;

@Builder
@Data
public class ReservationDTO {

	private Long id;
    private LocalDate date;
    private @ToString.Exclude ChildDTO child;
    private StopDTO departure;
    private StopDTO arrival;
    private boolean present;

    public static ReservationDTO fromEntity(ReservationEntity reservation, ChildDTO child) {
        return ReservationDTO.builder()
                .id(reservation.getId())
                .date(reservation.getDate())
                .child(child)
                .departure(StopDTO.fromEntity(
                        Utils.toDTO(reservation.getDeparture().getLine()),
                        reservation.getDeparture()))
                .arrival(StopDTO.fromEntity(
                        Utils.toDTO(reservation.getArrival().getLine()),
                        reservation.getArrival()))
                .present(reservation.isPresence())
                .build();
    }

    public static ReservationDTO fromEntity(ReservationEntity reservation) {
        ChildDTO c = ChildDTO.fromEntity(
                reservation.getChild(),
                UserDto.fromEntity(reservation.getChild().getParent()));
        return ReservationDTO.builder()
                .id(reservation.getId())
                .date(reservation.getDate())
                .departure(StopDTO.fromEntity(
                        Utils.toDTO(reservation.getDeparture().getLine()),
                        reservation.getDeparture()))
                .arrival(StopDTO.fromEntity(
                        Utils.toDTO(reservation.getArrival().getLine()),
                        reservation.getArrival()))
                .child(c)
                .present(reservation.isPresence())
                .build();
    }
}

package it.polito.ai.pedibus.service.dtos;

import it.polito.ai.pedibus.model.entities.StopEntity;
import it.polito.ai.pedibus.utils.Direction;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.LocalTime;

@Builder
@Data
public class StopDTO implements Comparable<StopDTO> {

    private Long id;
    private int sequence;
	private String name;
	private LocalTime time;
	@ToString.Exclude @EqualsAndHashCode.Exclude
	private LineDTO line;
	private double latitude;
	private double longitude;
	private Direction direction;

	@Override
	public int compareTo(StopDTO o) {
		return Integer.compare(this.sequence, o.sequence);
	}

	public static StopDTO fromEntity(LineDTO l, StopEntity s) {
		return StopDTO.builder()
				.id(s.getId())
				.sequence(s.getSequence())
				.name(s.getName())
				.time(s.getTime())
				.latitude(s.getLatitude())
				.longitude(s.getLongitude())
				.line(l)
				.direction(s.getDirection())
				.build();
	}
}

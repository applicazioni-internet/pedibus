package it.polito.ai.pedibus.model.repositories;

import it.polito.ai.pedibus.model.entities.MessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MessageRepository extends JpaRepository<MessageEntity, Long> {

    Optional<MessageEntity> findById(Long id);

    List<MessageEntity> findAllBySenderId(Long id);

    List<MessageEntity> findAllByRecipientId(Long id);
}

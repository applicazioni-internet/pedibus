package it.polito.ai.pedibus.model.repositories;

import it.polito.ai.pedibus.model.entities.ChildEntity;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChildRepository extends JpaRepository<ChildEntity, Long> {
    Optional<ChildEntity> findById(Long id);
}

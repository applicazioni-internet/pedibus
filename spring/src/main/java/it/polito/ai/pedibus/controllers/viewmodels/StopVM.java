package it.polito.ai.pedibus.controllers.viewmodels;

import it.polito.ai.pedibus.service.dtos.StopDTO;
import lombok.Builder;
import lombok.Data;

import java.time.LocalTime;

@Builder
@Data
public class StopVM {

    private long id;
	private String name;
	private double latitude;
    private double longitude;
    private LocalTime time;

    public static StopVM fromDto(StopDTO s) {
        return StopVM.builder()
                .id(s.getId())
                .name(s.getName())
                .time(s.getTime())
                .latitude(s.getLatitude())
                .longitude(s.getLongitude())
                .build();
    }
}

package it.polito.ai.pedibus.controllers;

import it.polito.ai.pedibus.controllers.viewmodels.UserPromotionRequest;
import it.polito.ai.pedibus.controllers.viewmodels.UserVM;
import it.polito.ai.pedibus.exceptions.UserNotFoundException;
import it.polito.ai.pedibus.service.UserService;
import it.polito.ai.pedibus.service.dtos.UserDto;
import it.polito.ai.pedibus.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.stream.Collectors;

import static it.polito.ai.pedibus.controllers.ControllerUtils.isPrivilegedUser;
import static it.polito.ai.pedibus.utils.Roles.ROLE_ADMINISTRATOR;
import static it.polito.ai.pedibus.utils.Roles.ROLE_SUPER_ADMINISTRATOR;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("api/users")
public class UserController {

	private final UserService userService;

	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;
	}

	@GetMapping("")
	@Secured({ROLE_ADMINISTRATOR, ROLE_SUPER_ADMINISTRATOR})
	public ResponseEntity<Resources<Resource<UserVM>>> getAllUsers() {
		return ResponseEntity.ok().body(new Resources<>(userService.getAllUsers().stream()
				.map(Utils::toVM)
				.sorted()
				.map(u -> new Resource<>(u,
						linkTo(methodOn(UserController.class)
								.getUserById(u.getId()))
								.withSelfRel()))
				.collect(Collectors.toList()),
				linkTo(methodOn(UserController.class)
						.getAllUsers())
						.withSelfRel()));
	}

	@GetMapping("/{id}")
	public ResponseEntity<Resource<UserVM>> getUserById(@PathVariable long id) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		UserDto user = (UserDto) authentication.getPrincipal();
		if (! isPrivilegedUser(user) && user.getId() != id) {
			final String message = "You must be a privileged user to retrieve this user";
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, message);
		}

		try {
		UserDto userDto = userService.loadUserById(id);
		return ResponseEntity.ok().body(new Resource<>(UserVM.fromDto(userDto),
				linkTo(methodOn(UserController.class)
						.getUserById(id))
						.withSelfRel()));
		} catch (UserNotFoundException e) {
			final String message = "User " + id + " not found";
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, message);
		}
	}

	@PutMapping("/{id}")
	@Secured({ROLE_ADMINISTRATOR, ROLE_SUPER_ADMINISTRATOR})
	public ResponseEntity<Resource<UserVM>> changeUserRole(
			@PathVariable long id,
			@RequestBody @Valid UserPromotionRequest request,
			BindingResult br) {

		if (br.hasErrors()) {
			final String message = ControllerUtils.getErrorMessages(br);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, message);
		}

		try {
			UserDto userDto = userService.promoteUserToAdmin(id, request.getPromote());
			return ResponseEntity.accepted().body(new Resource<>(UserVM.fromDto(userDto),
					linkTo(methodOn(UserController.class)
							.getUserById(id))
							.withSelfRel()));
		} catch (UserNotFoundException e) {
			final String message = "User " + id + " not found";
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, message);
		}
	}
}

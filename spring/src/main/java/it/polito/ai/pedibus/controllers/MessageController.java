package it.polito.ai.pedibus.controllers;

import it.polito.ai.pedibus.controllers.viewmodels.Message;
import it.polito.ai.pedibus.controllers.viewmodels.MessageUpdate;
import it.polito.ai.pedibus.exceptions.MessageNotFoundException;
import it.polito.ai.pedibus.exceptions.UserNotFoundException;
import it.polito.ai.pedibus.service.MessageService;
import it.polito.ai.pedibus.service.dtos.MessageDto;
import it.polito.ai.pedibus.service.dtos.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static it.polito.ai.pedibus.utils.Roles.ROLE_ADMINISTRATOR;
import static it.polito.ai.pedibus.utils.Roles.ROLE_SUPER_ADMINISTRATOR;

@RestController
@RequestMapping("api/messages")
public class MessageController {

    private final MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @PostMapping("")
    @Secured({ROLE_ADMINISTRATOR, ROLE_SUPER_ADMINISTRATOR})
    public ResponseEntity<Message> insertMessage(
            @RequestBody @Valid Message message,
            BindingResult br) {

        if (br.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        final UserDto user = (UserDto) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        MessageDto dto = MessageDto.builder()
                .body(message.getBody())
                .date(LocalDateTime.now())
                .recipient(message.getRecipient())
                .sender(user.getUsername())
                .build();

        try {
            dto = messageService.sendMessage(dto);
        } catch (UserNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
        }

        return ResponseEntity.ok(Message.fromDto(dto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Message> updateMessageRead(
            @PathVariable long id,
            @RequestBody @Valid MessageUpdate update,
            BindingResult br) {

        System.out.println("Update messageeee");
        System.out.println(update);
        if (br.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        final UserDto user = (UserDto) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        try {
            MessageDto msg = messageService.getMessageById(id);
            if (!msg.getRecipient().equals(user.getUsername())) {
                final String message = "You must be recipient of this message";
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, message);
            }
            msg = messageService.updateMessage(id, update.getRead());
            return ResponseEntity.accepted()
                    .body(Message.fromDto(msg));
        } catch (MessageNotFoundException e) {
            final String message = "Message " + id + " not found";
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, message);
        }
    }

    @GetMapping("/sent")
    @Secured({ROLE_ADMINISTRATOR, ROLE_SUPER_ADMINISTRATOR})
    public ResponseEntity<List<Message>> getAllMessagesFromUser() {
        final UserDto user = (UserDto) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<MessageDto> messages = messageService.getMessagesBySenderId(user.getId());
        return ResponseEntity.ok(messages.stream()
                .map(Message::fromDto)
                .sorted()
                .collect(Collectors.toList()));
    }

    @GetMapping("/received")
    public ResponseEntity<List<Message>> getAllMessagesToUser() {
        final UserDto user = (UserDto) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<MessageDto> messages = messageService.getMessagesByRecipientId(user.getId());
        return ResponseEntity.ok(messages.stream()
                .map(Message::fromDto)
                .sorted()
                .collect(Collectors.toList()));
    }
}

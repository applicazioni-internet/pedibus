package it.polito.ai.pedibus.controllers;

import it.polito.ai.pedibus.service.dtos.GrantedAuthorityImpl;
import it.polito.ai.pedibus.service.dtos.UserDto;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import java.util.stream.Collectors;

import static it.polito.ai.pedibus.utils.Roles.ROLE_ADMINISTRATOR;
import static it.polito.ai.pedibus.utils.Roles.ROLE_SUPER_ADMINISTRATOR;

@Component
class ControllerUtils {

    static boolean isPrivilegedUser(UserDto user) {
        return user.getAuthorities().contains(new GrantedAuthorityImpl(ROLE_ADMINISTRATOR)) ||
                user.getAuthorities().contains(new GrantedAuthorityImpl(ROLE_SUPER_ADMINISTRATOR));
    }

    static String getErrorMessages(BindingResult br) {
        return br.getFieldErrors().stream()
                .map(fe -> fe.getField() + " " + fe.getDefaultMessage())
                .collect(Collectors.joining(", "));
    }
}

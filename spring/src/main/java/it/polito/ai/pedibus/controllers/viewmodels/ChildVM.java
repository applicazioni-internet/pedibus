package it.polito.ai.pedibus.controllers.viewmodels;

import it.polito.ai.pedibus.service.dtos.ChildDTO;
import lombok.Builder;
import lombok.Value;

import java.time.LocalDate;

@Value
@Builder
public class ChildVM {

	Long id;
	String firstName;
    String lastName;
    LocalDate birthDate;

    public static ChildVM fromDto(ChildDTO child) {
        return ChildVM.builder()
                .id(child.getId())
                .firstName(child.getFirstName())
                .lastName(child.getLastName())
                .birthDate(child.getBirthDate())
                .build();
    }
}

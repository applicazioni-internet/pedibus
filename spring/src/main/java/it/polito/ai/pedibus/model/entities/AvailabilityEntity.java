package it.polito.ai.pedibus.model.entities;

import it.polito.ai.pedibus.service.dtos.AvailabilityDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "availability")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AvailabilityEntity {

    @Id
    @Column(updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "availability_generator")
    @SequenceGenerator(
            name = "availability_generator",
            sequenceName = "availability_sequence",
            allocationSize = 1
    )
    private Long id;

    @ManyToOne
    @JoinColumn(
            name = "line_id",
            nullable = false
    )
    private LineEntity line;

    @Column(nullable = false)
    private LocalDate date;

    @Column(nullable = false)
    private Boolean approved;

    @ManyToOne
    @JoinColumn(
            name = "user_id",
            nullable = false
    )
    private UserEntity user;

    public static AvailabilityEntity fromProposal(AvailabilityDto dto, LineEntity line, UserEntity user) {
        return AvailabilityEntity.builder()
                .approved(dto.isApproved())
                .date(dto.getDate())
                .line(line)
                .user(user)
                .build();
    }
}

--
-- PostgreSQL database dump
--

-- Dumped from database version 11.2
-- Dumped by pg_dump version 11.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: availability; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.availability (
    id bigint NOT NULL,
    approved boolean NOT NULL,
    date date NOT NULL,
    line_id bigint NOT NULL,
    user_id bigint NOT NULL
);


ALTER TABLE public.availability OWNER TO postgres;

--
-- Name: availability_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.availability_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.availability_sequence OWNER TO postgres;

--
-- Name: child; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.child (
    id bigint NOT NULL,
    birth_date date NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    parent_id bigint NOT NULL
);


ALTER TABLE public.child OWNER TO postgres;

--
-- Name: child_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.child_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.child_sequence OWNER TO postgres;

--
-- Name: line; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.line (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.line OWNER TO postgres;

--
-- Name: line_companion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.line_companion (
    line_id bigint NOT NULL,
    user_id bigint NOT NULL
);


ALTER TABLE public.line_companion OWNER TO postgres;

--
-- Name: line_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.line_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.line_sequence OWNER TO postgres;

--
-- Name: message; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.message (
    id bigint NOT NULL,
    body text,
    date timestamp without time zone NOT NULL,
    recipient_id bigint,
    sender_id bigint,
    read boolean NOT NULL
);


ALTER TABLE public.message OWNER TO postgres;

--
-- Name: message_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.message_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.message_sequence OWNER TO postgres;

--
-- Name: password_reset_token; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.password_reset_token (
    id bigint NOT NULL,
    expiring_date timestamp without time zone,
    token character varying(255),
    user_id bigint NOT NULL
);


ALTER TABLE public.password_reset_token OWNER TO postgres;

--
-- Name: password_reset_token_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.password_reset_token_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.password_reset_token_sequence OWNER TO postgres;

--
-- Name: reservation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reservation (
    id bigint NOT NULL,
    date date NOT NULL,
    presence boolean DEFAULT false,
    arrival_stop_id bigint NOT NULL,
    child_id bigint NOT NULL,
    departure_stop_id bigint NOT NULL
);


ALTER TABLE public.reservation OWNER TO postgres;

--
-- Name: reservation_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.reservation_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reservation_sequence OWNER TO postgres;

--
-- Name: role_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.role_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_sequence OWNER TO postgres;

--
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roles (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- Name: stop; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stop (
    id bigint NOT NULL,
    direction integer NOT NULL,
    latitude double precision NOT NULL,
    longitude double precision NOT NULL,
    name character varying(255) NOT NULL,
    sequence integer NOT NULL,
    "time" time without time zone NOT NULL,
    line_id bigint NOT NULL
);


ALTER TABLE public.stop OWNER TO postgres;

--
-- Name: stop_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.stop_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stop_sequence OWNER TO postgres;

--
-- Name: user_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_role (
    user_id bigint NOT NULL,
    role_id bigint NOT NULL
);


ALTER TABLE public.user_role OWNER TO postgres;

--
-- Name: user_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_sequence OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    email character varying(255) NOT NULL,
    enabled boolean NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    password character varying(255) NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: verification_token; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.verification_token (
    id bigint NOT NULL,
    expiring_date timestamp without time zone,
    token character varying(255),
    user_id bigint NOT NULL
);


ALTER TABLE public.verification_token OWNER TO postgres;

--
-- Name: verification_token_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.verification_token_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.verification_token_sequence OWNER TO postgres;

--
-- Name: 38067; Type: BLOB; Schema: -; Owner: postgres
--

SELECT pg_catalog.lo_create('38067');


ALTER LARGE OBJECT 38067 OWNER TO postgres;

--
-- Name: 38068; Type: BLOB; Schema: -; Owner: postgres
--

SELECT pg_catalog.lo_create('38068');


ALTER LARGE OBJECT 38068 OWNER TO postgres;

--
-- Name: 38069; Type: BLOB; Schema: -; Owner: postgres
--

SELECT pg_catalog.lo_create('38069');


ALTER LARGE OBJECT 38069 OWNER TO postgres;

--
-- Name: 38070; Type: BLOB; Schema: -; Owner: postgres
--

SELECT pg_catalog.lo_create('38070');


ALTER LARGE OBJECT 38070 OWNER TO postgres;

--
-- Name: 38071; Type: BLOB; Schema: -; Owner: postgres
--

SELECT pg_catalog.lo_create('38071');


ALTER LARGE OBJECT 38071 OWNER TO postgres;

--
-- Name: 38072; Type: BLOB; Schema: -; Owner: postgres
--

SELECT pg_catalog.lo_create('38072');


ALTER LARGE OBJECT 38072 OWNER TO postgres;

--
-- Name: 38073; Type: BLOB; Schema: -; Owner: postgres
--

SELECT pg_catalog.lo_create('38073');


ALTER LARGE OBJECT 38073 OWNER TO postgres;

--
-- Name: 38074; Type: BLOB; Schema: -; Owner: postgres
--

SELECT pg_catalog.lo_create('38074');


ALTER LARGE OBJECT 38074 OWNER TO postgres;

--
-- Name: 44717; Type: BLOB; Schema: -; Owner: postgres
--

SELECT pg_catalog.lo_create('44717');


ALTER LARGE OBJECT 44717 OWNER TO postgres;

--
-- Name: 44718; Type: BLOB; Schema: -; Owner: postgres
--

SELECT pg_catalog.lo_create('44718');


ALTER LARGE OBJECT 44718 OWNER TO postgres;

--
-- Name: 44719; Type: BLOB; Schema: -; Owner: postgres
--

SELECT pg_catalog.lo_create('44719');


ALTER LARGE OBJECT 44719 OWNER TO postgres;

--
-- Name: 44720; Type: BLOB; Schema: -; Owner: postgres
--

SELECT pg_catalog.lo_create('44720');


ALTER LARGE OBJECT 44720 OWNER TO postgres;

--
-- Name: 44721; Type: BLOB; Schema: -; Owner: postgres
--

SELECT pg_catalog.lo_create('44721');


ALTER LARGE OBJECT 44721 OWNER TO postgres;

--
-- Name: 44722; Type: BLOB; Schema: -; Owner: postgres
--

SELECT pg_catalog.lo_create('44722');


ALTER LARGE OBJECT 44722 OWNER TO postgres;

--
-- Name: 44723; Type: BLOB; Schema: -; Owner: postgres
--

SELECT pg_catalog.lo_create('44723');


ALTER LARGE OBJECT 44723 OWNER TO postgres;

--
-- Name: 44724; Type: BLOB; Schema: -; Owner: postgres
--

SELECT pg_catalog.lo_create('44724');


ALTER LARGE OBJECT 44724 OWNER TO postgres;

--
-- Name: 44725; Type: BLOB; Schema: -; Owner: postgres
--

SELECT pg_catalog.lo_create('44725');


ALTER LARGE OBJECT 44725 OWNER TO postgres;

--
-- Name: 44726; Type: BLOB; Schema: -; Owner: postgres
--

SELECT pg_catalog.lo_create('44726');


ALTER LARGE OBJECT 44726 OWNER TO postgres;

--
-- Data for Name: availability; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.availability (id, approved, date, line_id, user_id) FROM stdin;
1	t	2020-07-07	1	2
2	f	2020-07-07	1	3
3	f	2020-07-07	2	4
4	f	2020-07-07	2	5
5	f	2020-07-07	2	6
8	f	2020-07-04	1	2
7	t	2020-07-03	1	2
9	f	2020-07-03	2	2
10	f	2020-07-03	3	2
11	f	2020-07-05	1	2
12	f	2020-07-06	1	2
13	f	2020-07-03	1	3
14	f	2020-07-04	1	3
15	f	2020-07-05	1	3
16	f	2020-07-05	3	3
17	f	2020-07-06	3	3
18	f	2020-07-03	3	3
19	f	2020-07-03	2	4
20	f	2020-07-05	2	4
21	f	2020-07-03	1	5
22	f	2020-07-04	1	5
23	f	2020-07-05	1	5
24	f	2020-07-06	1	5
25	f	2020-07-07	1	5
26	f	2020-07-03	2	5
27	f	2020-07-05	2	5
28	f	2020-07-04	3	5
29	f	2020-07-03	3	5
30	f	2020-07-03	1	8
31	f	2020-07-04	1	8
32	f	2020-07-04	2	8
33	f	2020-07-04	3	8
34	f	2020-07-05	3	8
35	f	2020-07-06	3	8
36	f	2020-07-06	2	8
37	f	2020-07-06	1	8
\.


--
-- Data for Name: child; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.child (id, birth_date, first_name, last_name, parent_id) FROM stdin;
1	2012-07-14	Marino	Rossi	2
2	2011-07-14	Luigino	Rossi	3
3	2012-07-19	Luigina	Rossi	3
4	2012-07-14	Giovannino	Rossi	4
5	2012-08-08	Oliver	Doe	5
6	2013-09-09	Jack	Doe	5
7	2012-07-07	Amelia	Smith	6
8	2013-06-06	Olivia	Smith	6
9	2020-07-09	fsdg	sgdsf	7
10	2017-06-07	Pippuzzo	Bello	8
11	2020-01-30	Ciccio	Brienza	8
12	2020-04-07	jksvnslk	lksvmkl	9
\.


--
-- Data for Name: line; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.line (id, name) FROM stdin;
1	Bianco
2	Rosso
3	Verde
\.


--
-- Data for Name: line_companion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.line_companion (line_id, user_id) FROM stdin;
\.


--
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.message (id, body, date, recipient_id, sender_id, read) FROM stdin;
1	44717	2020-06-18 15:54:00.243128	3	5	f
3	44719	2020-06-18 15:54:39.295942	4	5	f
4	44720	2020-06-18 15:54:50.0178	6	5	f
5	44721	2020-06-18 15:56:50.968756	5	6	f
6	44722	2020-06-18 15:57:29.35512	2	6	f
7	44723	2020-06-18 15:57:37.975848	3	6	f
8	44724	2020-06-18 15:57:47.84688	4	6	f
9	44725	2020-06-18 15:58:23.642108	5	2	f
10	44726	2020-06-18 15:58:30.859326	6	2	f
2	38067	2020-06-18 15:54:12.708094	2	5	t
11	38068	2020-07-02 16:15:04.841228	2	2	f
12	38069	2020-07-02 16:15:06.833989	2	2	f
13	38070	2020-07-02 16:15:40.638175	2	2	f
14	38071	2020-07-02 20:35:15.257243	9	2	f
15	38072	2020-07-02 20:35:16.292477	9	2	f
16	38073	2020-07-02 20:35:25.39015	9	2	f
17	38074	2020-07-02 20:35:26.240875	8	2	f
\.


--
-- Data for Name: password_reset_token; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.password_reset_token (id, expiring_date, token, user_id) FROM stdin;
\.


--
-- Data for Name: reservation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.reservation (id, date, presence, arrival_stop_id, child_id, departure_stop_id) FROM stdin;
1	2020-06-07	t	15	1	1
2	2020-06-08	f	15	1	4
3	2020-06-09	t	10	1	3
4	2020-06-10	t	15	1	4
5	2020-06-11	t	15	1	1
6	2020-06-12	t	15	1	4
7	2020-06-13	f	15	1	1
8	2020-06-08	t	27	2	20
9	2020-06-08	f	27	3	22
88	2020-07-04	f	14	10	3
89	2020-07-05	f	15	10	11
90	2020-07-06	f	14	10	3
91	2020-07-03	f	15	11	2
92	2020-07-04	f	15	11	2
93	2020-07-05	f	15	11	11
94	2020-07-06	f	15	11	8
95	2020-07-06	f	12	11	3
96	2020-07-03	f	12	11	3
97	2020-07-04	f	12	11	3
98	2020-07-05	f	12	11	3
99	2020-07-08	f	14	1	3
15	2020-08-13	f	15	10	11
19	2020-08-11	f	15	10	2
25	2020-08-28	f	15	10	2
26	2020-07-14	f	15	10	1
28	2020-07-22	f	15	1	1
10	2020-07-01	f	15	1	2
29	2020-07-29	f	15	12	2
31	2020-07-30	f	39	1	34
32	2020-07-03	f	15	1	4
33	2020-07-04	f	15	1	4
34	2020-07-05	f	15	1	4
35	2020-07-06	f	15	1	4
36	2020-07-07	f	15	1	4
37	2020-07-08	f	15	1	4
38	2020-07-09	f	15	1	4
39	2020-07-10	f	15	1	4
40	2020-07-11	f	15	1	4
41	2020-07-03	f	15	3	11
42	2020-07-04	f	15	3	11
43	2020-07-05	f	15	3	11
44	2020-07-06	f	15	3	11
45	2020-07-07	f	15	3	11
46	2020-07-03	f	15	2	13
47	2020-07-04	f	15	2	13
48	2020-07-05	f	15	2	13
49	2020-07-06	f	15	2	13
50	2020-07-07	f	15	2	13
51	2020-07-08	f	15	2	13
52	2020-07-03	f	15	4	11
53	2020-07-04	f	15	4	11
54	2020-07-05	f	15	4	11
55	2020-07-05	f	27	4	18
56	2020-07-04	f	27	4	18
57	2020-07-03	f	27	4	18
59	2020-07-04	f	39	4	18
60	2020-07-03	f	5	4	3
61	2020-07-05	f	5	4	3
62	2020-07-06	f	5	4	3
63	2020-07-03	f	15	6	2
65	2020-07-04	f	15	6	2
66	2020-07-05	f	15	6	2
67	2020-07-06	f	15	6	2
68	2020-07-06	f	27	6	22
69	2020-07-05	f	24	6	3
70	2020-07-03	f	24	6	3
71	2020-07-04	f	27	6	22
72	2020-07-14	f	15	5	2
73	2020-07-03	f	15	5	2
74	2020-07-04	f	15	5	2
75	2020-07-05	f	15	5	2
76	2020-07-06	f	15	5	2
77	2020-07-03	f	3	5	3
78	2020-07-04	f	15	5	2
79	2020-07-05	f	15	5	2
80	2020-07-06	f	3	5	3
81	2020-07-03	f	15	10	11
82	2020-07-04	f	15	10	11
83	2020-07-05	f	15	10	11
84	2020-07-06	f	15	10	11
85	2020-07-07	f	15	10	11
86	2020-07-08	f	15	10	11
87	2020-07-03	f	14	10	3
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.roles (id, name) FROM stdin;
1	ROLE_SUPER_ADMIN
2	ROLE_ADMIN
3	ROLE_USER
\.


--
-- Data for Name: stop; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.stop (id, direction, latitude, longitude, name, sequence, "time", line_id) FROM stdin;
1	0	45.0636749267578125	7.67676162719726563	Porta Nuova Capolinea	0	12:25:00	1
2	0	45.0625	7.6769251823425293	Porta Nuova	1	12:30:00	1
3	1	45.0688018798828125	7.68859148025512695	Verdi	1	16:00:00	1
4	0	45.0615997314453125	7.67965459823608398	Carlo Alberto	2	12:35:00	1
5	1	45.0675735473632813	7.68768596649169922	Po	2	16:05:00	1
6	0	45.0611076354980469	7.68314599990844727	Vittorio Emanuele	3	12:45:00	1
7	1	45.0652732849121094	7.68608903884887695	Giolitti	3	16:15:00	1
8	0	45.0636215209960938	7.68491029739379883	Cavour	4	12:50:00	1
9	1	45.0636215209960938	7.68491029739379883	Cavour	4	16:25:00	1
10	1	45.0611076354980469	7.68314599990844727	Vittorio Emanuele	5	16:30:00	1
11	0	45.0652732849121094	7.68608903884887695	Giolitti	5	13:00:00	1
12	1	45.0615997314453125	7.67965459823608398	Carlo Alberto	6	16:40:00	1
13	0	45.0675735473632813	7.68768596649169922	Po	6	13:10:00	1
14	1	45.0625	7.6769251823425293	Porta Nuova	7	16:45:00	1
15	0	45.0688018798828125	7.68859148025512695	Verdi	7	13:15:00	1
16	1	45.0636749267578125	7.67676162719726563	Porta Nuova Capolinea	8	16:50:00	1
17	0	45.0625	7.6769251823425293	Porta Nuova	0	12:25:00	2
18	0	45.0615997314453125	7.67965459823608398	Carlo Alberto	1	12:30:00	2
19	1	45.0549888610839844	7.6826014518737793	Valentino	1	16:00:00	2
20	0	45.0606765747070313	7.68251705169677734	Madama Cristina	2	12:35:00	2
21	1	45.0566825866699219	7.68511152267456055	Pellico	2	16:10:00	2
22	0	45.0592727661132813	7.68662452697753906	Massimo D'Azeglio	3	12:45:00	2
23	1	45.0592727661132813	7.68662452697753906	Massimo D'Azeglio	3	16:15:00	2
24	1	45.0606765747070313	7.68251705169677734	Madama Cristina	4	16:25:00	2
25	0	45.0566825866699219	7.68511152267456055	Pellico	4	12:50:00	2
26	1	45.0615997314453125	7.67965459823608398	Carlo Alberto	5	16:30:00	2
27	0	45.0549888610839844	7.6826014518737793	Valentino	5	13:00:00	2
28	1	45.0625	7.6769251823425293	Porta Nuova	6	16:35:00	2
29	0	45.0627899169921875	7.67839765548706055	Porta Nuova FS	0	12:25:00	3
30	0	45.0631179809570313	7.67501020431518555	Vittorio Emanuele II	1	12:30:00	3
31	1	45.0630760192871094	7.66282558441162109	Politecnico	1	16:00:00	3
32	0	45.0625114440917969	7.67035579681396484	Re Umberto	2	12:35:00	3
33	1	45.0638656616210938	7.66285610198974609	Duca degli Abruzzi	2	16:10:00	3
34	0	45.0636177062988281	7.66716527938842773	Ferraris	3	12:45:00	3
35	1	45.0636177062988281	7.66716527938842773	Ferraris	3	16:15:00	3
36	1	45.0625114440917969	7.67035579681396484	Re Umberto	4	16:25:00	3
37	0	45.0638656616210938	7.66285610198974609	Duca degli Abruzzi	4	12:50:00	3
38	1	45.0631179809570313	7.67501020431518555	Vittorio Emanuele II	5	16:30:00	3
39	0	45.0630760192871094	7.66282558441162109	Politecnico	5	13:00:00	3
40	1	45.0627899169921875	7.67839765548706055	Porta Nuova FS	6	16:35:00	3
\.


--
-- Data for Name: user_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_role (user_id, role_id) FROM stdin;
1	1
1	2
1	3
2	2
2	3
3	2
3	3
4	2
4	3
5	3
6	3
7	3
8	3
9	3
8	2
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, email, enabled, first_name, last_name, password) FROM stdin;
1	superuser@mail.it	t	Super	User	$2a$10$h0UEqnhmu0LC.PZWGsf.7.OkU9vTk5j3PeDnyn4i7YeUqPB0b2Hyi
2	mario.rossi@mail.it	t	Mario	Rossi	$2a$10$Inb9XREoLceY/U49cirRrO5BOsdMyRBlI4ntIcoHRaTXtEYHffONm
3	luigi.verdi@mail.it	t	Luigi	Verdi	$2a$10$/2hJVeDKGsTcYdwGzzfxf.Y2NJRixEF9xVvllCrgmOztxMiArbpHq
4	giovanni.bianchi@mail.it	t	Giovanni	Bianchi	$2a$10$lVI/GGdmIaMFfXq4DdBP8uyfHxZY2jTXlikKl9/AvI.mXGCbB.jTu
5	john.doe@mail.it	t	John	Doe	$2a$10$9Xu/hV.PzUVenChfUnEpKeYWUxJpUFrwh4I6PCm8MQsBsmxQsG22S
6	jane.doe@mail.it	t	Jane	Doe	$2a$10$d5DbtPPRJl.vRuFfwJI5DOtQM26OqCflzfFNEdGgB9dnI0gTxdAgy
7	sjdfk@mail.it	f	jkfnlk	kosfno	$2a$10$su/QedxmIajjlDSRGZD.e.u2x65URc5TxrQRPs7yTWHzcDn69skqa
8	new@mail.it	t	Pippo	Sowlo	$2a$10$x3mCSEekaIVUHs4v6mqXk.pnkDh/FherbljqLNT2Rr1SIAgp348vK
9	test@mail.it	t	Pippo	Sowlo	$2a$10$n2m0HqQIyjV6ZPxbJhPeY.H/Ns4ExB.CMHdSI/YmcWRpwC5ezb7pa
\.


--
-- Data for Name: verification_token; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.verification_token (id, expiring_date, token, user_id) FROM stdin;
1	2020-07-01 17:47:48.126	ae2244e0-2cad-4f00-89d0-7bcde57d6e8e	7
\.


--
-- Name: availability_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.availability_sequence', 37, true);


--
-- Name: child_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.child_sequence', 12, true);


--
-- Name: line_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.line_sequence', 3, true);


--
-- Name: message_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.message_sequence', 17, true);


--
-- Name: password_reset_token_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.password_reset_token_sequence', 1, false);


--
-- Name: reservation_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.reservation_sequence', 99, true);


--
-- Name: role_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.role_sequence', 3, true);


--
-- Name: stop_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.stop_sequence', 40, true);


--
-- Name: user_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_sequence', 9, true);


--
-- Name: verification_token_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.verification_token_sequence', 3, true);


--
-- Data for Name: BLOBS; Type: BLOBS; Schema: -; Owner: 
--

BEGIN;

SELECT pg_catalog.lo_open('44717', 131072);
SELECT pg_catalog.lowrite(0, '\x48656c6c6f212054686973206973206d79206669727374206d65737361676521');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('44718', 131072);
SELECT pg_catalog.lowrite(0, '\x48656c6c6f212054686973206973206d79207365636f6e64206d65737361676521');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('44719', 131072);
SELECT pg_catalog.lowrite(0, '\x48656c6c6f212054686973206973206d79207468697264206d65737361676521');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('44720', 131072);
SELECT pg_catalog.lowrite(0, '\x48656c6c6f212054686973206973206d7920666f75727468206d65737361676521');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('44721', 131072);
SELECT pg_catalog.lowrite(0, '\x48656c6c6f212054686973206973206d79206669727374207265706c7921');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('44722', 131072);
SELECT pg_catalog.lowrite(0, '\x48656c6c6f212054686973206973206d79206669727374206d65737361676521');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('44723', 131072);
SELECT pg_catalog.lowrite(0, '\x48656c6c6f212054686973206973206d79207365636f6e64206d65737361676521');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('44724', 131072);
SELECT pg_catalog.lowrite(0, '\x48656c6c6f212054686973206973206d79207468697264206d65737361676521');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('44725', 131072);
SELECT pg_catalog.lowrite(0, '\x48656c6c6f212054686973206973206d79206669727374207265706c7921');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('44726', 131072);
SELECT pg_catalog.lowrite(0, '\x48656c6c6f212054686973206973206d79207365636f6e64207265706c7921');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('38067', 131072);
SELECT pg_catalog.lowrite(0, '\x48656c6c6f212054686973206973206d79207365636f6e64206d65737361676521');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('38068', 131072);
SELECT pg_catalog.lowrite(0, '\x4e6f7720796f75206172652074686520636f6d70616e696f6e20666f72206c696e65203120696e206461746520323032302d30372d3033');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('38069', 131072);
SELECT pg_catalog.lowrite(0, '\x596f757220617661696c6162696c69747920666f72206c696e65203120696e206461746520323032302d30372d3033686173206265656e2063616e63656c6c6564');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('38070', 131072);
SELECT pg_catalog.lowrite(0, '\x4e6f7720796f75206172652074686520636f6d70616e696f6e20666f72206c696e65203120696e206461746520323032302d30372d3033');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('38071', 131072);
SELECT pg_catalog.lowrite(0, '\x596f7520617265206e6f772061646d696e202121');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('38072', 131072);
SELECT pg_catalog.lowrite(0, '\x596f7520617265206e6f772061646d696e202121');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('38073', 131072);
SELECT pg_catalog.lowrite(0, '\x796f7520617265206e6f206c6f6e67657220616e2061646d696e2e202121');
SELECT pg_catalog.lo_close(0);

SELECT pg_catalog.lo_open('38074', 131072);
SELECT pg_catalog.lowrite(0, '\x596f7520617265206e6f772061646d696e202121');
SELECT pg_catalog.lo_close(0);

COMMIT;

--
-- Name: availability availability_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.availability
    ADD CONSTRAINT availability_pkey PRIMARY KEY (id);


--
-- Name: child child_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.child
    ADD CONSTRAINT child_pkey PRIMARY KEY (id);


--
-- Name: line_companion line_companion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.line_companion
    ADD CONSTRAINT line_companion_pkey PRIMARY KEY (line_id, user_id);


--
-- Name: line line_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.line
    ADD CONSTRAINT line_pkey PRIMARY KEY (id);


--
-- Name: message message_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT message_pkey PRIMARY KEY (id);


--
-- Name: password_reset_token password_reset_token_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.password_reset_token
    ADD CONSTRAINT password_reset_token_pkey PRIMARY KEY (id);


--
-- Name: reservation reservation_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservation
    ADD CONSTRAINT reservation_pkey PRIMARY KEY (id);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: stop stop_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.stop
    ADD CONSTRAINT stop_pkey PRIMARY KEY (id);


--
-- Name: line uk_9ney9davbulf79nmn9vg6k7tn; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.line
    ADD CONSTRAINT uk_9ney9davbulf79nmn9vg6k7tn UNIQUE (name);


--
-- Name: roles uk_ofx66keruapi6vyqpv6f2or37; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT uk_ofx66keruapi6vyqpv6f2or37 UNIQUE (name);


--
-- Name: user_role user_role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_role
    ADD CONSTRAINT user_role_pkey PRIMARY KEY (user_id, role_id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: verification_token verification_token_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.verification_token
    ADD CONSTRAINT verification_token_pkey PRIMARY KEY (id);


--
-- Name: stop fk138il65kmhlxrjgcxd62ipu1j; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.stop
    ADD CONSTRAINT fk138il65kmhlxrjgcxd62ipu1j FOREIGN KEY (line_id) REFERENCES public.line(id);


--
-- Name: verification_token fk3asw9wnv76uxu3kr1ekq4i1ld; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.verification_token
    ADD CONSTRAINT fk3asw9wnv76uxu3kr1ekq4i1ld FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: line_companion fk43gl3uj22gfw7mbdnsuve9t9w; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.line_companion
    ADD CONSTRAINT fk43gl3uj22gfw7mbdnsuve9t9w FOREIGN KEY (line_id) REFERENCES public.line(id);


--
-- Name: availability fk7t6k99hita4ctxwh8qvgsc9s7; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.availability
    ADD CONSTRAINT fk7t6k99hita4ctxwh8qvgsc9s7 FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: password_reset_token fk83nsrttkwkb6ym0anu051mtxn; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.password_reset_token
    ADD CONSTRAINT fk83nsrttkwkb6ym0anu051mtxn FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: reservation fka5n75hgfouwiq88ljb1txsnll; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservation
    ADD CONSTRAINT fka5n75hgfouwiq88ljb1txsnll FOREIGN KEY (departure_stop_id) REFERENCES public.stop(id);


--
-- Name: message fkbi5avhe69aol2mb1lnm6r4o2p; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT fkbi5avhe69aol2mb1lnm6r4o2p FOREIGN KEY (sender_id) REFERENCES public.users(id);


--
-- Name: message fkc60hib4mr1pl61lmcpvehy3m5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT fkc60hib4mr1pl61lmcpvehy3m5 FOREIGN KEY (recipient_id) REFERENCES public.users(id);


--
-- Name: reservation fkchf1oey27ynepn0gnnt8wv7tw; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservation
    ADD CONSTRAINT fkchf1oey27ynepn0gnnt8wv7tw FOREIGN KEY (child_id) REFERENCES public.child(id);


--
-- Name: availability fkdruxtc0k9ibnjta7mgymhfh9a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.availability
    ADD CONSTRAINT fkdruxtc0k9ibnjta7mgymhfh9a FOREIGN KEY (line_id) REFERENCES public.line(id);


--
-- Name: line_companion fkg02sky179s2etwfh3na419ish; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.line_companion
    ADD CONSTRAINT fkg02sky179s2etwfh3na419ish FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: user_role fkj345gk1bovqvfame88rcx7yyx; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_role
    ADD CONSTRAINT fkj345gk1bovqvfame88rcx7yyx FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: reservation fkm116pj26d9pm8oeuy9igny4cc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reservation
    ADD CONSTRAINT fkm116pj26d9pm8oeuy9igny4cc FOREIGN KEY (arrival_stop_id) REFERENCES public.stop(id);


--
-- Name: child fkr3mmdq8r4ggk5rcnmfeoa8e6a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.child
    ADD CONSTRAINT fkr3mmdq8r4ggk5rcnmfeoa8e6a FOREIGN KEY (parent_id) REFERENCES public.users(id);


--
-- Name: user_role fkt7e7djp752sqn6w22i6ocqy6q; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_role
    ADD CONSTRAINT fkt7e7djp752sqn6w22i6ocqy6q FOREIGN KEY (role_id) REFERENCES public.roles(id);


--
-- PostgreSQL database dump complete
--

